	<script>
	<#if reportData??>
		<#if reportData.row?? && reportData.row.title?size gt 0 && reportData.col?? && reportData.col.title?size gt 0 && reportData.data?? && reportData.data?size gt 0>
			var data_${element.mid!''} = [];
			var mea_${element.mid!''} = [];
			<#list reportData.row.title as tl>
				<#assign inx = 0>
				<#list tl as title>
					var obj_${element.mid!''} = new Object();
						obj_${element.mid!''}["_name"] = "${title.name!''}";
						<#if title.valueData??>
							<#list title.valueData as val>
								
								<#if val.valueStyle!val.foramatValue?? && val.valueStyle!val.foramatValue != ''>
									obj_${element.mid!''}["${val.name!''}"] = ${val.valueStyle!val.foramatValue!''};
								<#else>
									obj_${element.mid!''}["${val.name!''}"] = 0;
								</#if>
									<#if inx == 0>
										mea_${element.mid!''}.push('${val.name!''}');	
									</#if>
								
							</#list>
						</#if>
					<#assign inx = inx + 1>		
					data_${element.mid!''}.push(obj_${element.mid!''});							
				</#list>
					
			</#list>

		<#elseif reportData.col?? && reportData.col.title?size gt 0 && reportData.data?? && reportData.data?size gt 0>
			var data_${element.mid!''} = [];
			var mea_${element.mid!''} = [];
				<#list reportData.col.title as title>
					<#if (title_index+1) == reportData.col.title?size>
							
						<#list title as rowtl>	
										var obj_${element.mid!''} = new Object();
										obj_${element.mid!''}["_name"] = "${rowtl.rename!rowtl.name!''}";
																
										<#if reportData?? && reportData.data??>
											<#list reportData.data as values>
												
													<#list values as val>	
														<#if rowtl_index == val_index>
																
															
															<#if val.foramatValue?? && val.foramatValue != ''>
																obj_${element.mid!''}["${rowtl.rename!rowtl.name!''}"] = ${val.valueStyle!val.foramatValue!''}
															<#else>
																obj_${element.mid!''}["${rowtl.rename!rowtl.name!''}"] = 0
															</#if>
															
														</#if>
													</#list>
												
											</#list>
										</#if>
										
										data_${element.mid!''}.push(obj_${element.mid!''});						
											
							mea_${element.mid!''}.push('${rowtl.rename!rowtl.name!''}');						
						</#list>
					</#if>
				</#list>
			
		<#else>

		const data_${element.mid!''} = [
			{ _name:'London', 'Jan.': 18.9, 'Feb.': 28.8, 'Mar.' :39.3, 'Apr.': 81.4, 'May': 47, 'Jun.': 20.3, 'Jul.': 24, 'Aug.': 35.6 },
			{ _name:'Berlin', 'Jan.': 12.4, 'Feb.': 23.2, 'Mar.' :34.5, 'Apr.': 99.7, 'May': 52.6, 'Jun.': 35.5, 'Jul.': 37.4, 'Aug.': 42.4}
		  ];
		const mea_${element.mid!''} =  [ 'Jan.','Feb.','Mar.','Apr.','May','Jun.','Jul.','Aug.' ];
		</#if>
		
	<#else>

		const data_${element.mid!''} = [
			{ _name:'London', 'Jan.': 18.9, 'Feb.': 28.8, 'Mar.' :39.3, 'Apr.': 81.4, 'May': 47, 'Jun.': 20.3, 'Jul.': 24, 'Aug.': 35.6 },
			{ _name:'Berlin', 'Jan.': 12.4, 'Feb.': 23.2, 'Mar.' :34.5, 'Apr.': 99.7, 'May': 52.6, 'Jun.': 35.5, 'Jul.': 37.4, 'Aug.': 42.4}
		  ];
		const mea_${element.mid!''} =  [ 'Jan.','Feb.','Mar.','Apr.','May','Jun.','Jul.','Aug.' ];
	</#if>
	
		const ds_${element.mid!''} = new DataSet();
	  const dv_${element.mid!''} = ds_${element.mid!''}.createView().source(data_${element.mid!''});
	  dv_${element.mid!''}.transform({
		type: 'fold',
		fields: mea_${element.mid!''}, // 展开字段集
		key: 'key', // key字段
		value: 'value', // value字段
	  });
		var json_${element.mid!''} =  $.parseJSON( $("#json_${element.mid!''}").text() );
	  $("#json_${element.mid!''}").remove();
	  ChartAction.renderChart('${element.mid!''}',dv_${element.mid!''},json_${element.mid!''})
	  	  	console.info(data_${element.mid!''})
	</script>
