ALTER TABLE uk_spt_salespatter ADD event tinyint(4) DEFAULT 0 COMMENT '是否挂机保存采集数据';
ALTER TABLE uk_que_survey_process ADD event tinyint(4) DEFAULT 0 COMMENT '是否挂机保存采集数据';


ALTER TABLE uk_columnproperties ADD mgroup tinyint(4) DEFAULT 0 COMMENT '启用分组';

CREATE TABLE `uk_cdr_bleg` (
  `creater` varchar(50) DEFAULT NULL COMMENT '创建人',
  `updater` varchar(50) DEFAULT NULL COMMENT '更新人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `data_status` tinyint(4) DEFAULT '0' COMMENT '是否删除',
  `id` varchar(255) NOT NULL COMMENT 'uuid',
  `call_uuid` varchar(255) DEFAULT NULL COMMENT 'call_uuid',
  `caller_id_name` varchar(255) DEFAULT NULL,
  `caller_id_number` varchar(255) DEFAULT NULL,
  `destination_number` varchar(255) DEFAULT NULL,
  `start_stamp` datetime DEFAULT NULL,
  `answer_stamp` datetime DEFAULT NULL,
  `end_stamp` datetime DEFAULT NULL,
  `uduration` bigint(20) DEFAULT NULL,
  `duration` int(12) DEFAULT NULL,
  `mduration` int(12) DEFAULT NULL,
  `billsec` int(12) DEFAULT NULL,
  `hangup_cause` varchar(255) DEFAULT NULL,
  `network_addr` varchar(255) DEFAULT NULL,
  `ani` varchar(255) DEFAULT NULL,
  `direction` varchar(255) DEFAULT NULL,
  `nibble_rate` decimal(12,4) DEFAULT '0.0000' COMMENT '话费rate',
  `nibble_total_billed` decimal(12,4) DEFAULT '0.0000' COMMENT '总费用',
  `nibble_current_balance` decimal(12,4) DEFAULT '0.0000' COMMENT '当次通话剩余话费',
  `nibble_account` varchar(255) DEFAULT NULL COMMENT '当前账户',
  `call_direction` varchar(255) DEFAULT NULL,
  `nibble_increment` decimal(12,4) DEFAULT '0.0000',
  `nibble_minimum` decimal(12,4) DEFAULT '0.0000',
  `orgi` varchar(50) DEFAULT NULL,
  `ringsec` int(12) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_101` (`orgi`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='话单表';


ALTER TABLE uk_agentuser ADD ordertime datetime DEFAULT NULL COMMENT '排队时间';


CREATE TABLE `uk_callcenter_event_history` (
  `ID` varchar(100) NOT NULL COMMENT '主键ID',
  `NAME` varchar(50) DEFAULT NULL COMMENT '名称',
  `CODE` varchar(50) DEFAULT NULL COMMENT '代码',
  `CREATETIME` datetime DEFAULT NULL COMMENT '创建时间',
  `CREATER` varchar(32) DEFAULT NULL COMMENT '创建人',
  `UPDATETIME` datetime DEFAULT NULL COMMENT '更新时间',
  `ORGI` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `USERNAME` varchar(50) DEFAULT NULL COMMENT '用户名',
  `SOURCE` varchar(50) DEFAULT NULL COMMENT '来源',
  `ANSWER` varchar(50) DEFAULT NULL COMMENT '应答时间',
  `scurrent` tinyint(4) DEFAULT NULL COMMENT '是否当前通话',
  `INIT` tinyint(4) DEFAULT NULL COMMENT '初始通话',
  `CALLER` varchar(50) DEFAULT NULL COMMENT '呼叫发起号码',
  `CALLING` varchar(50) DEFAULT NULL COMMENT '呼叫对象',
  `CALLED` varchar(50) DEFAULT NULL COMMENT '被叫号码',
  `AGENTYPE` varchar(50) DEFAULT NULL COMMENT '坐席类型',
  `QUENE` varchar(50) DEFAULT NULL COMMENT '队列名称',
  `ANI` varchar(50) DEFAULT NULL COMMENT '主叫号码',
  `TOUSER` varchar(50) DEFAULT NULL COMMENT '目标用户',
  `DIRECTION` varchar(50) DEFAULT NULL COMMENT '呼叫方向',
  `STATE` varchar(50) DEFAULT NULL COMMENT '状态',
  `AGENT` varchar(50) DEFAULT NULL COMMENT '坐席工号',
  `ACTION` varchar(50) DEFAULT NULL COMMENT '事件动作',
  `HOST` varchar(50) DEFAULT NULL COMMENT '时间主机',
  `IPADDR` varchar(50) DEFAULT NULL COMMENT '主机IP',
  `LOCALDATETIME` varchar(50) DEFAULT NULL COMMENT '时间发起时间',
  `STATUS` varchar(50) DEFAULT NULL COMMENT '状态代码',
  `TIME` decimal(20,0) DEFAULT NULL COMMENT '时间秒值',
  `STARTTIME` datetime DEFAULT NULL COMMENT '通话开始时间',
  `ENDTIME` datetime DEFAULT NULL COMMENT '通话结束时间',
  `DURATION` int(11) DEFAULT NULL COMMENT '通话时长',
  `INSIDE` tinyint(4) DEFAULT NULL COMMENT '内线',
  `MISSCALL` tinyint(4) DEFAULT NULL COMMENT '是否漏话',
  `srecord` tinyint(4) DEFAULT NULL COMMENT '是否录音',
  `RECORDTIME` int(11) DEFAULT NULL COMMENT '录音时长',
  `STARTRECORD` datetime DEFAULT NULL COMMENT '开始录音时间',
  `ENDRECORD` datetime DEFAULT NULL COMMENT '结束录音时间',
  `ANSWERTIME` datetime DEFAULT NULL COMMENT '应答时间',
  `RINGDURATION` int(11) DEFAULT NULL COMMENT '振铃时长',
  `SERVICESUMMARY` tinyint(4) DEFAULT NULL COMMENT '是否记录服务小结',
  `SERVICEID` varchar(32) DEFAULT NULL COMMENT '服务记录ID',
  `RECORDFILE` varchar(255) DEFAULT NULL COMMENT '录音文件名',
  `CALLBACK` tinyint(4) DEFAULT NULL COMMENT '回呼',
  `CCQUENE` varchar(50) DEFAULT NULL COMMENT '转接队列',
  `SERVICESTATUS` varchar(20) DEFAULT NULL COMMENT '当前呼叫状态',
  `CHANNELSTATUS` varchar(50) DEFAULT NULL COMMENT '事件中的呼叫状态',
  `COUNTRY` varchar(50) DEFAULT NULL COMMENT '来电国家',
  `PROVINCE` varchar(50) DEFAULT NULL COMMENT '来电号码归属省份',
  `CITY` varchar(50) DEFAULT NULL COMMENT '来电归属号码城市',
  `ISP` varchar(50) DEFAULT NULL COMMENT '来电号码运营商',
  `VOICECALLED` varchar(50) DEFAULT NULL COMMENT '语音呼叫',
  `contactsid` varchar(50) DEFAULT NULL COMMENT '联系人ID',
  `EXTENTION` varchar(32) DEFAULT NULL COMMENT '分机ID',
  `HOSTID` varchar(32) DEFAULT NULL COMMENT 'PBX服务器ID',
  `CALLTYPE` varchar(20) DEFAULT NULL COMMENT '呼叫方向类型|计费类型',
  `CALLDIR` varchar(30) DEFAULT NULL COMMENT '呼叫方向',
  `OTHERDIR` varchar(30) DEFAULT NULL COMMENT '对边呼叫方向',
  `OTHERLEGDEST` varchar(50) DEFAULT NULL COMMENT '呼叫另一方号码',
  `BRIDGEID` varchar(100) DEFAULT NULL COMMENT '桥接ID',
  `BRIDGE` tinyint(4) DEFAULT NULL COMMENT '是否有桥接',
  `RECORDFILENAME` varchar(255) DEFAULT NULL COMMENT '褰曢煶鏂囦欢鍚',
  `DISCALLER` varchar(50) DEFAULT NULL COMMENT '显示主叫',
  `DISCALLED` varchar(50) DEFAULT NULL COMMENT '显示被叫',
  `SATISF` tinyint(4) DEFAULT '0' COMMENT '满意度',
  `SATISFACTION` varchar(32) DEFAULT NULL COMMENT '满意度结果',
  `SATISFDATE` datetime DEFAULT NULL COMMENT '满意度时间',
  `datestr` varchar(32) DEFAULT '0' COMMENT '坐席通话日期（yyyy-MM-dd）用于每小时通话数量折线图',
  `hourstr` varchar(32) DEFAULT '0' COMMENT '坐席通话时间小时（HH）用于每小时通话数量折线图',
  `taskid` varchar(32) DEFAULT NULL COMMENT '任务ID',
  `actid` varchar(32) DEFAULT NULL COMMENT '活动ID',
  `batid` varchar(32) DEFAULT NULL COMMENT '批次ID',
  `dataid` varchar(50) DEFAULT NULL COMMENT '鏁版嵁ID',
  `statustype` varchar(32) DEFAULT NULL COMMENT '号码隐藏状态',
  `disphonenum` varchar(32) DEFAULT NULL COMMENT '号码',
  `distype` varchar(32) DEFAULT NULL COMMENT '显示类型',
  `nameid` varchar(50) DEFAULT NULL COMMENT '名单ID',
  `siptrunk` varchar(32) DEFAULT NULL COMMENT '拨打的网关',
  `prefix` tinyint(4) DEFAULT '0' COMMENT '是否在号码前加拨0',
  `userid` varchar(32) DEFAULT NULL COMMENT '坐席用户ID',
  `organ` varchar(32) DEFAULT NULL COMMENT '坐席用户所属部门',
  `busstype` varchar(32) DEFAULT NULL COMMENT '业务类型',
  `metaname` varchar(50) DEFAULT NULL COMMENT '名单数据表名',
  `waste` tinyint(4) DEFAULT '0' COMMENT '是否作废名单',
  `callstatus` tinyint(4) DEFAULT '0' COMMENT '拨打状态',
  `apstatus` tinyint(4) DEFAULT '0' COMMENT '是否预约名单拨打',
  `qualitystatus` varchar(20) DEFAULT NULL COMMENT '质检状态',
  `qualitydisorgan` varchar(32) DEFAULT NULL COMMENT '分配的质检部门',
  `qualitydisuser` varchar(32) DEFAULT NULL COMMENT '分配的质检用户',
  `qualityorgan` varchar(32) DEFAULT NULL COMMENT '实际质检部门',
  `qualityuser` varchar(32) DEFAULT NULL COMMENT '实际质检人',
  `qualityscore` int(11) DEFAULT '0' COMMENT '质检得分',
  `qualitytime` datetime DEFAULT NULL COMMENT '质检时间',
  `qualitytype` varchar(20) DEFAULT NULL COMMENT '质检类型',
  `assuser` varchar(50) DEFAULT NULL COMMENT '分配执行人',
  `templateid` varchar(50) DEFAULT NULL COMMENT '质检模板id',
  `qualitydistime` datetime DEFAULT NULL COMMENT '质检分配的时间',
  `qualitydistype` varchar(32) DEFAULT NULL COMMENT '分配状态  ，未分配not/分配到部门disorgan/分配到坐席disagent',
  `qualityactid` varchar(50) DEFAULT NULL COMMENT '质检活动id',
  `qualityfilterid` varchar(50) DEFAULT NULL COMMENT '筛选表单id',
  `transbegin` datetime DEFAULT NULL COMMENT '语音转写开始时间',
  `transend` datetime DEFAULT NULL COMMENT '语音转写结束时间',
  `transtime` varchar(32) DEFAULT NULL COMMENT '语音转写用时',
  `trans` tinyint(4) DEFAULT '0' COMMENT '是否语音转写（0未转写1已转写）',
  `transtatus` varchar(32) DEFAULT NULL COMMENT '语音转写状态',
  `transcost` tinyint(4) DEFAULT '0' COMMENT '语音转写费用',
  `engine` varchar(32) DEFAULT NULL COMMENT '语音转写引擎',
  `qualitypass` tinyint(4) DEFAULT '2' COMMENT '质检是否合格(默认2为未质检)',
  `tranid` varchar(50) DEFAULT NULL COMMENT '语音转写任务ID',
  `forecast` tinyint(4) DEFAULT '0' COMMENT '预测式外呼',
  `skill` varchar(50) DEFAULT NULL COMMENT '外呼队列',
  `forecastid` varchar(50) DEFAULT NULL COMMENT '外呼队列ID',
  `callresult` varchar(500) DEFAULT NULL COMMENT '拨打结果信息',
  `dtmf` tinyint(4) DEFAULT '0' COMMENT '记录DTMF事件',
  `dtmfrec` varchar(255) DEFAULT NULL COMMENT 'DTMF记录',
  `hangupcase` varchar(32) DEFAULT NULL COMMENT '挂断原因',
  `hangupinitiator` varchar(32) DEFAULT NULL COMMENT '挂断方',
  `workstatus` varchar(32) DEFAULT NULL COMMENT '名单业务状态',
  `sipaddr` varchar(50) DEFAULT NULL COMMENT '客户端IP',
  `autoanswer` tinyint(4) DEFAULT '0' COMMENT '是否自动接听',
  `ossstatus` varchar(4) DEFAULT '0' COMMENT '//录音文件是否上传到oss 0否 1是 2文件不存在',
  `coreuuid` varchar(50) DEFAULT NULL COMMENT 'FS标识',
  `inqueuetime` datetime DEFAULT NULL COMMENT '进入队列时间',
  `outqueuetime` datetime DEFAULT NULL COMMENT '出队列时间',
  `queuetime` int(11) DEFAULT '0' COMMENT '排队时长',
  `membersessionid` varchar(50) DEFAULT NULL COMMENT '转接前通话ID',
  `itemid` varchar(50) DEFAULT NULL COMMENT '项目ID',
  `qualityresult` varchar(50) DEFAULT NULL COMMENT '质检结果',
  `accountdes` varchar(255) DEFAULT NULL COMMENT '瀹㈡埛璐﹀彿淇℃伅',
  `conference` tinyint(4) DEFAULT '0' COMMENT '是否会议通话',
  `inconferenecetime` datetime DEFAULT NULL COMMENT '进入会议时间',
  `conferenceduration` int(11) DEFAULT '0' COMMENT '会议时长',
  `conferencenum` varchar(50) DEFAULT '0' COMMENT '会议号码',
  `conferenceid` varchar(50) DEFAULT NULL COMMENT '会议ID',
  `conferenceinitiator` tinyint(4) DEFAULT '0' COMMENT '是否会议发起方',
  `level` varchar(50) DEFAULT NULL COMMENT '话术评价等级',
  `levelscore` int(11) DEFAULT '0' COMMENT '话术评价分数',
  `focustimes` int(11) DEFAULT '0' COMMENT '话术关注点次数',
  `processid` varchar(50) DEFAULT NULL COMMENT '话术or问卷id',
  `gatewaycount` tinyint(4) DEFAULT '0' COMMENT '网关已经计数',
  `gateway` varchar(100) DEFAULT NULL COMMENT '网关名称',
  `aitrans` tinyint(4) DEFAULT '0' COMMENT '机器人转接',
  `aitransqus` varchar(50) DEFAULT NULL COMMENT '转接问题ID',
  `aitranstime` datetime DEFAULT NULL COMMENT '机器人转接时间',
  `aitransduration` int(11) DEFAULT '0' COMMENT '机器人转接是通话时长',
  `aiext` varchar(50) DEFAULT NULL COMMENT '机器人号码',
  `ai` tinyint(4) DEFAULT '0' COMMENT '是否机器人会话',
  `aieventid` varchar(50) DEFAULT NULL COMMENT '转接前通话记录ID',
  `asrtimes` int(11) DEFAULT '0' COMMENT 'asr次数',
  `ttstimes` int(11) DEFAULT '0' COMMENT 'tts次数',
  `timeouttimes` int(11) DEFAULT '0' COMMENT '当前节点超时次数',
  `errortimes` int(11) DEFAULT '0' COMMENT '当前节点错误次数',
  `nmlinetimes` int(11) DEFAULT '0' COMMENT '非主线节点次数',
  `waittime` int(11) DEFAULT '0' COMMENT '等待时长',
  `transfaild` tinyint(4) DEFAULT '0' COMMENT '人工转接失败',
  `priphone` varchar(50) DEFAULT NULL COMMENT '隐私号码',
  `transcon` tinyint(4) DEFAULT '0' COMMENT '是否条件转',
  `transconid` varchar(50) DEFAULT NULL COMMENT '转接条件',
  `business` varchar(255) DEFAULT NULL COMMENT '业务需求类型（联系人状态）',
  `busmemo` varchar(255) DEFAULT NULL COMMENT '业务需求说明（联系人说明）',
  `video` tinyint(4) DEFAULT '0' COMMENT '是否视频呼叫',
  `videoip` varchar(50) DEFAULT NULL COMMENT '视频呼叫IP',
  `videorec` varchar(200) DEFAULT NULL COMMENT '视频录像文件地址',
  `videorectime` int(11) DEFAULT '0' COMMENT '视频录像时长',
  `videocodec` varchar(50) DEFAULT NULL COMMENT '视频编码',
  `webimuserid` varchar(50) DEFAULT NULL COMMENT 'WebIM用户ID',
  `webimusername` varchar(50) DEFAULT NULL COMMENT 'WebIM用户名称',
  `con_quality` varchar(50) DEFAULT NULL,
  `con_qualitypass` varchar(50) DEFAULT NULL,
  `con_invitation` varchar(50) DEFAULT NULL,
  `igr` tinyint(4) DEFAULT '0' COMMENT '是否进行性别年龄识别',
  `igrage` varchar(50) DEFAULT NULL COMMENT '识别引擎返回 - 表示识别的年龄[0：middle(12~40岁) 1：child（0~12岁）2：old（40岁以上）]',
  `igrchild` varchar(50) DEFAULT NULL COMMENT '识别引擎返回 - 表示识别为儿童的概率值，儿童、中年、老年概率值最大的为最终结果	',
  `igrmiddle` varchar(50) DEFAULT NULL COMMENT '识别引擎返回 - 表示识别为中年的概率值，儿童、中年、老年概率值最大的为最终结果	',
  `igrold` varchar(50) DEFAULT NULL COMMENT '识别引擎返回 - 表示识别为老年的概率值，儿童、中年、老年概率值最大的为最终结果	',
  `igrgender` varchar(50) DEFAULT NULL COMMENT '识别引擎返回 - 表示识别的性别 [0：女性 1：男性]',
  `igrfemale` varchar(50) DEFAULT NULL COMMENT '识别引擎返回 - 表示识别为女声的概率值，女声、男声概率值较大的为最终结果',
  `igrmale` varchar(50) DEFAULT NULL COMMENT '识别引擎返回 - 表示识别为男声的概率值，女声、男声概率值较大的为最终结',
  `igrbegin` datetime DEFAULT NULL COMMENT '开始识别时间',
  `igrend` datetime DEFAULT NULL COMMENT '结束识别时间',
  `con_qualitysubmit` varchar(50) DEFAULT NULL,
  `aicollect` tinyint(4) DEFAULT '0' COMMENT '标记该通话，是否是全流程机器人采集数据',
  `igrvoice` varchar(255) DEFAULT NULL COMMENT '性别年龄识别 - 语音文件',
  `igrvoicetime` int(11) DEFAULT '0' COMMENT '性别年龄识别 - 语音时长',
  `igrstatus` varchar(50) DEFAULT NULL COMMENT '性别年龄识别 - 当前状态',
  `con_surnames` varchar(255) DEFAULT NULL COMMENT '联系人姓名',
  `con_intention` varchar(50) DEFAULT NULL COMMENT '意向度',
  `synces` tinyint(4) DEFAULT '0' COMMENT '是否完成同步数据',
  `aitransans` varchar(50) DEFAULT NULL COMMENT '答案ID',
  `irgvoicetime` int(11) DEFAULT '0' COMMENT '鎬у埆骞撮緞璇嗗埆 - 璇?煶鏃堕暱',
  `irgstatus` varchar(50) DEFAULT NULL COMMENT '鎬у埆骞撮緞璇嗗埆 - 褰撳墠鐘舵?',
  `autoquality` tinyint(4) DEFAULT '0' COMMENT '鏄?惁鑷?姩璐ㄦ?',
  `spotqc` tinyint(4) DEFAULT '0' COMMENT '是否抽检',
  `spotqctime` datetime DEFAULT NULL COMMENT '抽检时间',
  `spotqcsuccess` tinyint(4) DEFAULT '0' COMMENT '抽检是否成功',
  `appealqc` tinyint(4) DEFAULT '0' COMMENT '是否申诉质检',
  `arbitrateqc` tinyint(4) DEFAULT '0' COMMENT '是否仲裁质检',
  PRIMARY KEY (`ID`) USING BTREE,
  KEY `history_index_transtatus_transbegin` (`transtatus`,`transbegin`) USING BTREE,
  KEY `history_index_200` (`CODE`) USING BTREE,
  KEY `history_index_201` (`datestr`) USING BTREE,
  KEY `history_index_202` (`hourstr`) USING BTREE,
  KEY `history_index_203` (`contactsid`) USING BTREE,
  KEY `history_index_204` (`ORGI`) USING BTREE,
  KEY `history_index_205` (`transtatus`) USING BTREE,
  KEY `history_index_206` (`transbegin`) USING BTREE,
  KEY `history_index_207` (`SERVICESTATUS`) USING BTREE,
  KEY `history_index_208` (`CREATETIME`) USING BTREE,
  KEY `history_index_209` (`STARTTIME`) USING BTREE,
  KEY `history_index_210` (`DISCALLED`) USING BTREE,
  KEY `history_index_211` (`DISCALLER`) USING BTREE,
  KEY `history_index_212` (`srecord`) USING BTREE,
  KEY `history_index_214` (`SATISFACTION`) USING BTREE,
  KEY `history_index_215` (`SATISF`) USING BTREE,
  KEY `history_index_216` (`MISSCALL`) USING BTREE,
  KEY `history_index_301` (`gateway`) USING BTREE,
  KEY `history_index_302` (`gatewaycount`) USING BTREE,
  KEY `history_index_303` (`DURATION`) USING BTREE,
  KEY `history_index_304` (`RINGDURATION`) USING BTREE,
  KEY `history_index_305` (`CALLTYPE`) USING BTREE,
  KEY `history_index_306` (`DIRECTION`) USING BTREE,
  KEY `history_index_307` (`userid`) USING BTREE,
  KEY `history_index_308` (`organ`) USING BTREE,
  KEY `history_index_309` (`CREATETIME`) USING BTREE,
  KEY `history_index_310` (`STARTTIME`) USING BTREE,
  KEY `history_index_311` (`nameid`) USING BTREE,
  KEY `history_index_312` (`actid`) USING BTREE,
  KEY `history_index_313` (`membersessionid`) USING BTREE,
  KEY `history_index_314` (`metaname`) USING BTREE,
  KEY `history_index_315` (`aitrans`) USING BTREE,
  KEY `history_index_316` (`ai`) USING BTREE,
  KEY `history_index_317` (`srecord`) USING BTREE,
  KEY `history_index_321` (`aieventid`) USING BTREE,
  KEY `history_index_320` (`USERNAME`) USING BTREE,
  KEY `history_index_322` (`datestr`) USING BTREE,
  KEY `history_index_318` (`con_qualitypass`) USING BTREE,
  KEY `history_index_323` (`AGENT`) USING BTREE,
  KEY `history_index_324` (`asrtimes`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='通话记录表';

INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c948a8671ef2cab0171ef30ba030015', '历史通话', 'pub', 'A10_A04', 'ukewo', 'layui-icon', '402881ef612b1f5b01612cc626f90547', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2020-05-07 20:51:20', NULL, '1', '0', '402888815d2fe37f015d2fe75cc80002', '0', '0', NULL, NULL, NULL, NULL, NULL, '0');
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c948a8671ef2cab0171ef313376001a', '我的通话', 'pub', 'A10_A04_A01', 'ukewo', 'layui-icon', '2c948a8671ef2cab0171ef30ba030015', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2020-05-07 20:51:51', NULL, '1', '0', '402888815d2fe37f015d2fe75cc80002', '0', '0', NULL, NULL, NULL, NULL, NULL, '0');
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c948a8671ef2cab0171ef316caf001e', '历史通话', 'pub', 'A10_A04_A02', 'ukewo', 'layui-icon', '2c948a8671ef2cab0171ef30ba030015', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2020-05-07 20:52:05', NULL, '1', '0', '402888815d2fe37f015d2fe75cc80002', '0', '0', NULL, NULL, NULL, NULL, NULL, '0');
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c948a8671ef2cab0171ef32320b0023', '录音管理', 'pub', 'A10_A04_A04', 'ukewo', 'layui-icon', '2c948a8671ef2cab0171ef30ba030015', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2020-05-07 20:52:56', NULL, '1', '0', '402888815d2fe37f015d2fe75cc80002', '0', '0', NULL, NULL, NULL, NULL, NULL, '0');
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c948a8671ef2cab0171ef3264020027', '语音留言', 'pub', 'A10_A04_A05', 'ukewo', 'layui-icon', '2c948a8671ef2cab0171ef30ba030015', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2020-05-07 20:53:09', NULL, '1', '0', '402888815d2fe37f015d2fe75cc80002', '0', '0', NULL, NULL, NULL, NULL, NULL, '0');
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c948a8671ef2cab0171ef329b67002c', '漏话列表', 'pub', 'A10_A04_A06', 'ukewo', 'layui-icon', '2c948a8671ef2cab0171ef30ba030015', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2020-05-07 20:53:23', NULL, '1', '0', '402888815d2fe37f015d2fe75cc80002', '0', '0', NULL, NULL, NULL, NULL, NULL, '0');


ALTER TABLE uk_act_callnames ADD calltype VARCHAR(50) DEFAULT NULL COMMENT '外呼类型';

ALTER TABLE uk_systemconfig ADD dumpcall tinyint(4) DEFAULT 0 COMMENT '是否启用 转储 语音通话';
ALTER TABLE uk_systemconfig ADD dumpcalldays int(11) DEFAULT 0 COMMENT '转储保留时间';
ALTER TABLE uk_systemconfig ADD dumpstarthour int(11) DEFAULT 0 COMMENT '转储开始时间（小时）';
ALTER TABLE uk_systemconfig ADD dumpstartmin int(11) DEFAULT 0 COMMENT '转储开始时间（分钟）';
ALTER TABLE uk_systemconfig ADD dumpendhour int(11) DEFAULT 0 COMMENT '转储结束时间（小时）';
ALTER TABLE uk_systemconfig ADD dumpendmin int(11) DEFAULT 0 COMMENT '转储结束时间（分钟）';

ALTER TABLE uk_callcenter_event_history ADD (extdata1 varchar(50) DEFAULT NULL COMMENT '名单扩展字段1' , extdata2 varchar(50) DEFAULT NULL COMMENT '名单扩展字段2' , extdata3 varchar(50) DEFAULT NULL COMMENT '名单扩展字段3');

ALTER TABLE uk_callcenter_event ADD (extdata1 varchar(50) DEFAULT NULL COMMENT '名单扩展字段1' , extdata2 varchar(50) DEFAULT NULL COMMENT '名单扩展字段2' , extdata3 varchar(50) DEFAULT NULL COMMENT '名单扩展字段3');
ALTER TABLE uk_callcenter_event_history ADD (extdata4 varchar(50) DEFAULT NULL COMMENT '名单扩展字段4' , extdata5 varchar(50) DEFAULT NULL COMMENT '名单扩展字段5' , extdata6 varchar(50) DEFAULT NULL COMMENT '名单扩展字段6');

ALTER TABLE uk_callcenter_event ADD (extdata4 varchar(50) DEFAULT NULL COMMENT '名单扩展字段4' , extdata5 varchar(50) DEFAULT NULL COMMENT '名单扩展字段5' , extdata6 varchar(50) DEFAULT NULL COMMENT '名单扩展字段6');
ALTER TABLE uk_spt_salespatter ADD result tinyint(4) DEFAULT 0 COMMENT '是否保存结果';
ALTER TABLE uk_que_survey_process ADD result tinyint(4) DEFAULT 0 COMMENT '是否保存结果';

INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880e77230f92601723151e91b00d6', '外线通话导出记录', 'pub', 'A10_A01_A09_B04', 'ukewo', 'layui-icon', '402881ef612b1f5b01612cee4fbb058a', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2020-05-20 17:02:31', NULL, '1', '0', '402888815d2fe37f015d2fe75cc80002', '0', '1', NULL, NULL, NULL, NULL, NULL, '0');
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880e77230f92601723156c61000e1', '智能监控', 'pub', 'A20', 'ukewo', 'layui-icon', '402888815d2fe37f015d2fe75cc80002', '', NULL, '', '', 'webim', '4028cac3614cd2f901614cf8be1f0324', '2020-05-20 17:07:49', NULL, '1', '0', '402888815d2fe37f015d2fe75cc80002', '0', '0', '/apps/monitor/index.html', NULL, NULL, NULL, NULL, '0');
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880e77230f92601723157971700ed', '外呼监控', 'pub', 'A20_A01', 'ukewo', 'layui-icon', '402880e77230f92601723156c61000e1', '', NULL, '', '', 'webim', '4028cac3614cd2f901614cf8be1f0324', '2020-05-20 17:08:43', NULL, '1', '0', '402888815d2fe37f015d2fe75cc80002', '0', '1', '/apps/monitor/callout.html', NULL, NULL, NULL, NULL, '0');
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880e77230f926017231583dc00101', '人工座席监控', 'pub', 'A20_A02', 'ukewo', 'layui-icon', '402880e77230f92601723156c61000e1', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2020-05-20 17:09:26', NULL, '1', '0', '402888815d2fe37f015d2fe75cc80002', '0', '1', NULL, NULL, NULL, NULL, NULL, '0');
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880e77230f926017231589ab80109', '外呼机器人监控', 'pub', 'A20_A03', 'ukewo', 'layui-icon', '402880e77230f92601723156c61000e1', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2020-05-20 17:09:49', NULL, '1', '0', '402888815d2fe37f015d2fe75cc80002', '0', '1', NULL, NULL, NULL, NULL, NULL, '0');
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880e77230f92601723163b19c01c7', '坐席监控导出', 'pub', 'A20_A02_B01', 'ukewo', 'layui-icon', '402880e77230f92601723156c61000e1', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2020-05-20 17:21:56', NULL, '1', '0', '402888815d2fe37f015d2fe75cc80002', '0', '1', NULL, NULL, NULL, NULL, NULL, '0');
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880e77230f92601723163e4a601cd', '机器人监控导出', 'pub', 'A20_A03_B01', 'ukewo', 'layui-icon', '402880e77230f92601723156c61000e1', '', NULL, '', '', 'webim', '4028cac3614cd2f901614cf8be1f0324', '2020-05-20 17:22:09', NULL, '1', '0', '402888815d2fe37f015d2fe75cc80002', '0', '1', NULL, NULL, NULL, NULL, NULL, '0');
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880e77230f9260172317af54c029f', '外线通话导出', 'pub', 'A10_A04_A03_B01', 'ukewo', 'layui-icon', '2c948a8671ef2cab0171ef30ba030015', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2020-05-20 17:47:21', NULL, '1', '0', '402888815d2fe37f015d2fe75cc80002', '0', '0', NULL, NULL, NULL, NULL, NULL, '0');
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880e772360c0c01723626df6e0147', '预测式外呼监控', 'pub', 'A20_A04', 'ukewo', 'layui-icon', '402880e77230f92601723156c61000e1', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2020-05-21 15:33:36', NULL, '1', '0', '402888815d2fe37f015d2fe75cc80002', '0', '1', NULL, NULL, NULL, NULL, NULL, '0');
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880e772360c0c0172362991b20172', '预测式外呼监控坐席监控导出', 'pub', 'A20_A04_B01', 'ukewo', 'layui-icon', '402880e77230f92601723156c61000e1', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2020-05-21 15:36:33', NULL, '1', '0', '402888815d2fe37f015d2fe75cc80002', '0', '1', NULL, NULL, NULL, NULL, NULL, '0');
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('402880e772360c0c01723629e965017a', '预测式外呼监控技能组监控导出', 'pub', 'A20_A04_B02', 'ukewo', 'layui-icon', '402880e77230f92601723156c61000e1', '', NULL, '', '', 'webim', '4028cac3614cd2f901614cf8be1f0324', '2020-05-21 15:36:55', NULL, '1', '0', '402888815d2fe37f015d2fe75cc80002', '0', '1', NULL, NULL, NULL, NULL, NULL, '0');

ALTER TABLE uk_callcenter_pbxhost ADD hidephonenum tinyint(4) DEFAULT 0 COMMENT '是否隐藏号码';
ALTER TABLE uk_callcenter_pbxhost ADD distype varchar(50) DEFAULT NULL COMMENT '号码隐藏方式';
ALTER TABLE uk_qc_config ADD accesskey varchar(50) DEFAULT NULL COMMENT '阿里云AccessKey';


ALTER TABLE uk_qc_template_item ADD ctarget VARCHAR(32) DEFAULT NULL COMMENT '对象类型';
ALTER TABLE uk_qc_result_item ADD ctarget VARCHAR(32) DEFAULT NULL COMMENT '对象类型';
ALTER TABLE uk_qc_result_item ADD mchit VARCHAR(200) DEFAULT NULL COMMENT '必须包含-命中词';
ALTER TABLE uk_qc_result_item ADD nchit VARCHAR(200) DEFAULT NULL COMMENT '不可包含-命中词';


ALTER TABLE uk_callcenter_event_history ADD con_bankcard VARCHAR(255) DEFAULT NULL ;
ALTER TABLE uk_callcenter_event_history ADD con_city VARCHAR(255) DEFAULT NULL COMMENT '联系人城市';
ALTER TABLE uk_callcenter_event_history ADD con_datetendency VARCHAR(50) DEFAULT NULL COMMENT '日期倾向';
ALTER TABLE uk_callcenter_event_history ADD con_education VARCHAR(50) DEFAULT NULL COMMENT '学历是否高于大专';
ALTER TABLE uk_callcenter_event_history ADD con_position VARCHAR(50) DEFAULT NULL COMMENT '职位';
ALTER TABLE uk_callcenter_event_history ADD con_qualitysituation VARCHAR(50) DEFAULT NULL COMMENT '质检原因';
ALTER TABLE uk_callcenter_event_history ADD contactname VARCHAR(255) DEFAULT NULL COMMENT '联系人姓名';
ALTER TABLE uk_callcenter_event_history ADD contactgender VARCHAR(50) DEFAULT NULL COMMENT '联系人性别';
ALTER TABLE uk_callcenter_event_history ADD con_servicelife VARCHAR(50) DEFAULT NULL COMMENT '他行信用卡使用一年以上'; 
ALTER TABLE uk_callcenter_event_history ADD con_work VARCHAR(50) DEFAULT NULL COMMENT '是否有工作';
ALTER TABLE uk_callcenter_event_history ADD failurereason VARCHAR(50) DEFAULT NULL COMMENT '失败原因';
ALTER TABLE uk_callcenter_event_history ADD notsubmitreason VARCHAR(50) DEFAULT NULL COMMENT '不可提交原因';
ALTER TABLE uk_callcenter_event_history ADD result VARCHAR(255) DEFAULT NULL COMMENT '复核结果';


INSERT INTO `uk_sysdic`(`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`, `tenant`) VALUES ('2c9a58e6724f1e4c0172594ada3f42a0', '阿里云', 'pub', 'aliphonetic', 'ukewo', 'layui-icon', '297e740666d3bbd30166d3c979880024', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2020-05-28 11:19:37', NULL, 1, 1, '297e740666d3bbd30166d3c979880024', 0, 0, NULL, NULL, NULL, NULL, NULL, 0);

ALTER TABLE uk_systemconfig ADD dumpnoanswer tinyint(4) DEFAULT 0 COMMENT '是否包含未接通数据';

ALTER TABLE uk_callcenter_event ADD aitransans varchar(50) DEFAULT NULL COMMENT '答案ID';
ALTER TABLE uk_callcenter_event ADD autoquality tinyint DEFAULT 0 COMMENT '是否自动质检';
ALTER TABLE uk_act_callnames ADD aitransans varchar(50) DEFAULT NULL COMMENT '答案ID';
ALTER TABLE uk_act_callnames ADD conid VARCHAR(50) DEFAULT NULL COMMENT '联系人ID';

ALTER TABLE uk_act_callnames ADD forecast tinyint(4) DEFAULT 0 COMMENT '预测式通外呼通话';

ALTER TABLE uk_servicesummary ADD `conductor` varchar(50) DEFAULT NULL COMMENT '处理人（业务）';
ALTER TABLE uk_servicesummary ADD `conductoruname` varchar(255) DEFAULT NULL COMMENT '处理人展示名（用户账号的uname）';
ALTER TABLE uk_servicesummary ADD `processstatus` varchar(50) DEFAULT NULL COMMENT '业务状态（字典项com.dic.rx.processstatus）';
