package com.ukefu.webim.config.web;

import com.corundumstudio.socketio.SocketIOChannelInitializer;

import io.netty.channel.Channel;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.timeout.IdleStateHandler;

/**
 * 
 * @author iceworld
 *
 */
public class UKeFuSocketIOChannelInitializer extends SocketIOChannelInitializer{
	
	@Override
	public void initChannel(Channel ch) {
		ChannelPipeline pipeline = ch.pipeline();
        addSslHandler(pipeline);
        addSocketioHandlers(pipeline);
        pipeline.addFirst(new IdleStateHandler(30, 0, 0)).addLast(new UKeFuIdleStateTrigger()) ;
	}
}
