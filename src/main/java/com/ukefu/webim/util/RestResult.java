package com.ukefu.webim.util;

import com.ukefu.webim.web.model.Extention;

public class RestResult implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 783408637220961119L;
	private RestResultType status ;
	
	private String token ;
	
	private Extention extention ;
	
	
	public RestResult(RestResultType status){
		this.status = status ;
	}
	
	public Object data ;
	public Object msg;
	
	public RestResult(RestResultType status , Object data){
		this.status = status ;
		this.data = data ;
	}
	
	public RestResult(RestResultType status  , String token , Object data,Extention extention ){
		this.status = status ;
		this.token = token ;
		this.extention = extention ;
		this.data = data ;
	}
	

	public RestResultType getStatus() {
		return status;
	}

	public void setStatus(RestResultType status) {
		this.status = status;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public Object getMsg() {
		return msg;
	}

	public void setMsg(Object msg) {
		this.msg = msg;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Extention getExtention() {
		return extention;
	}

	public void setExtention(Extention extention) {
		this.extention = extention;
	}
	
	
}
