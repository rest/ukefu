package com.ukefu.webim.util.disruptor.ai;

import com.ukefu.util.event.AiEvent;
import com.ukefu.webim.web.model.MessageOutContent;

public interface AiMessageProcesserInterface {
	public MessageOutContent onEvent(AiEvent event, long arg1, boolean arg2) ;
}
