package com.ukefu.webim.util.disruptor;

import java.util.HashMap;

import org.apache.commons.lang3.StringUtils;

import com.lmax.disruptor.EventHandler;
import com.ukefu.core.UKDataContext;
import com.ukefu.util.UKTools;
import com.ukefu.util.asr.lfasr.PhoneticTranscription;
import com.ukefu.util.event.UserDataEvent;
import com.ukefu.util.mail.Mail;
import com.ukefu.util.sms.CommandSms;
import com.ukefu.webim.service.es.OnlineUserESRepository;
import com.ukefu.webim.service.es.UserEventRepository;
import com.ukefu.webim.service.repository.ChatMessageRepository;
import com.ukefu.webim.service.repository.RequestLogRepository;
import com.ukefu.webim.service.repository.UserTraceRepository;
import com.ukefu.webim.util.server.message.ChatMessage;
import com.ukefu.webim.web.model.OnlineUser;
import com.ukefu.webim.web.model.QualityConfig;
import com.ukefu.webim.web.model.RequestLog;
import com.ukefu.webim.web.model.SmsResult;
import com.ukefu.webim.web.model.StatusEvent;
import com.ukefu.webim.web.model.SystemConfig;
import com.ukefu.webim.web.model.UserHistory;
import com.ukefu.webim.web.model.UserTraceHistory;


public class UserEventHandler implements EventHandler<UserDataEvent>{
	
	@Override
	public void onEvent(UserDataEvent arg0, long arg1, boolean arg2)
			throws Exception {
		if(arg0.getEvent() instanceof UserHistory){
			UserEventRepository userEventRes = UKDataContext.getContext().getBean(UserEventRepository.class) ;
			userEventRes.save((UserHistory)arg0.getEvent()) ;
		}else if(arg0.getEvent() instanceof UserTraceHistory){
			UserTraceRepository userTraceRes = UKDataContext.getContext().getBean(UserTraceRepository.class) ;
			userTraceRes.save((UserTraceHistory)arg0.getEvent()) ;
		}else if(arg0.getEvent() instanceof RequestLog){
			RequestLogRepository requestLogRes = UKDataContext.getContext().getBean(RequestLogRepository.class) ;
			requestLogRes.save((RequestLog)arg0.getEvent()) ;
		}else if(arg0.getEvent() instanceof OnlineUser){
			OnlineUserESRepository onlineUserRes = UKDataContext.getContext().getBean(OnlineUserESRepository.class) ;
			onlineUserRes.save((OnlineUser)arg0.getEvent()) ;
		}if(arg0.getEvent() instanceof StatusEvent){
			StatusEvent statusEvent = (StatusEvent)arg0.getEvent() ;
			QualityConfig qConfig = UKTools.initQualityConfig(statusEvent.getOrgi()) ;
			SystemConfig systemConfig = UKTools.getSystemConfig();
			if(qConfig.isPhonetrans() && !StringUtils.isBlank(qConfig.getEngine()) && systemConfig != null && !StringUtils.isBlank(systemConfig.getIconstr())) {
				PhoneticTranscription trans = (PhoneticTranscription) UKDataContext.getContext().getBean(qConfig.getEngine()) ;
				if(trans!=null) {
					trans.getLfasr(statusEvent.getUserid(), statusEvent.getOrgi(), statusEvent.getOrgan(), statusEvent, qConfig);
				}
			}
		}else if(arg0.getEvent() instanceof Mail){
			Mail mail = (Mail)arg0.getEvent() ;
			if(null!=mail&&!StringUtils.isBlank(mail.getEmail())) {
				UKTools.sendMail(mail.getEmail(), mail.getCc(), mail.getSubject(), mail.getContent(), mail.getFilenames());
			}
		}else if(arg0.getEvent() instanceof CommandSms){
			CommandSms commandSms = (CommandSms)arg0.getEvent() ;
			if(null!=commandSms && !StringUtils.isBlank(commandSms.getMobile()) &&  !StringUtils.isBlank(commandSms.getSmsid())) {
				SmsResult result = new SmsResult();
				result.setUserid(commandSms.getUserid());
				result.setTemplettype(commandSms.getType());
				result.setOrgi(commandSms.getOrgi());
				UKTools.sendSmsByTemplate(commandSms.getMobile(), null, commandSms.getSmsid(), commandSms.getuKDataBean()!=null ? commandSms.getuKDataBean().getValues() : new HashMap<String, Object>(), result);
			}
		}else if(arg0.getEvent() instanceof ChatMessage){
			ChatMessageRepository chatMessageRes = UKDataContext.getContext().getBean(ChatMessageRepository.class) ;
			chatMessageRes.save((ChatMessage)arg0.getEvent()) ;
		}
	}

}
