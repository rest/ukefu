package com.ukefu.webim.util.disruptor.ai;

import com.lmax.disruptor.EventFactory;
import com.ukefu.util.event.AiEvent;

public class AiEventFactory implements EventFactory<AiEvent>{

	@Override
	public AiEvent newInstance() {
		return new AiEvent();
	}
}
