package com.ukefu.webim.util.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ukefu.webim.service.es.MRoundQuestionRepository;
import com.ukefu.webim.util.event.AiMRoundsProcesser;
import com.ukefu.webim.util.server.message.ChatMessage;
import com.ukefu.webim.web.model.AiUser;
import com.ukefu.webim.web.model.MRoundQuestion;

@Component
public class AiMRoundsProcesserImpl extends AiMRoundsProcesser{
	
	@Autowired
	private MRoundQuestionRepository mRoundsQuestionRes ;

	@Override
	public ChatMessage process(AiUser aiUser , ChatMessage chat, String question) {
		List<MRoundQuestion> mRoundsQuestionList = mRoundsQuestionRes.findByDataidAndOrgi(question , !StringUtils.isBlank(aiUser.getOrgi()) ? aiUser.getOrgi() : chat.getOrgi()) ;
		MRoundQuestion current = null ;
		if(mRoundsQuestionList!=null) {
			for(MRoundQuestion mrq : mRoundsQuestionList) {
				if(mrq.getId().equals(mrq.getDataid())) {
					int delay = 200 ;
					if("1".equals(mrq.getUserask()) && !StringUtils.isBlank(mrq.getDelay()) && mrq.getDelay().matches("[\\d]{1,2}")) {
						delay = Integer.parseInt(mrq.getDelay())*1000 ;
					}
					replayMessage(current = mrq , mrq.getContent() , chat , hasChilde(mRoundsQuestionList, current) == false, "nochild" , delay , aiUser);
					
					aiUser.setTopictransnum(aiUser.getTopictransnum()+1);		//增加话题转换次数
					break ;
				}
			}
		}
		return super.exchange(aiUser , chat , current , null , hasChilde(mRoundsQuestionList, current)) ;
	}
}
