package com.ukefu.webim.service.quene;

import java.util.Map;

import com.hazelcast.query.Predicate;
import com.ukefu.webim.web.model.StatusEvent;

public class StatusEventFilterPredicate implements Predicate<String, StatusEvent> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1236581634096258855L;
	private String orgi ;
	/**
	 * 
	 */
	public StatusEventFilterPredicate(String orgi){
		this.orgi = orgi ;
	}
	public boolean apply(Map.Entry<String, StatusEvent> mapEntry) {
		return orgi.equals(mapEntry.getValue().getOrgi());
	}
}