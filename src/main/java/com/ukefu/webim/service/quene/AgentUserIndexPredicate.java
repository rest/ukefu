package com.ukefu.webim.service.quene;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.hazelcast.query.Predicate;
import com.ukefu.webim.web.model.AgentUser;

/**
 * @author liuyonghong
 *
 */
public class AgentUserIndexPredicate implements Predicate<String, AgentUser> {

	private static final long serialVersionUID = 1236581634096258855L;
	private String orgi;
	private long ordertime ;
	private String status;

	public AgentUserIndexPredicate() {
	}

	public AgentUserIndexPredicate(String orgi, String status ,long ordertime) {
		this.orgi = orgi;
		this.status = status;
		this.ordertime = ordertime ;
	}

	@Override
	public boolean apply(Map.Entry<String, AgentUser> mapEntry) {
		AgentUser agentUser = mapEntry.getValue() ;
		return agentUser!=null && agentUser.getStatus()!=null && !StringUtils.isBlank(orgi) && orgi.equals(agentUser.getOrgi()) && agentUser.getStatus()!=null && agentUser.getStatus().equals(status) && agentUser.getOrdertime()!=null ? agentUser.getOrdertime().getTime() < ordertime : 0 < ordertime;
	}
}
