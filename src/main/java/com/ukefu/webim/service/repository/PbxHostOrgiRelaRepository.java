package com.ukefu.webim.service.repository;

import com.ukefu.webim.web.model.PbxHostOrgiRela;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PbxHostOrgiRelaRepository extends JpaRepository<PbxHostOrgiRela, String> {

	public List<PbxHostOrgiRela> findByOrgi(String orgi);

	public List<PbxHostOrgiRela> findByPbxhostid(String hostid);
	
	public List<PbxHostOrgiRela> findByPbxhostidAndOrgi(String hostid,String orgi);
}