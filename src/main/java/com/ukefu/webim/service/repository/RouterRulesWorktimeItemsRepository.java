package com.ukefu.webim.service.repository;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.RouterRulesWorktimeItems;

public interface RouterRulesWorktimeItemsRepository extends JpaRepository<RouterRulesWorktimeItems, String> {
	
	public RouterRulesWorktimeItems findByIdAndOrgi(String id , String orgi);
	
	public List<RouterRulesWorktimeItems> findAll(Specification<RouterRulesWorktimeItems> spec) ;
	
	public RouterRulesWorktimeItems findById(String id);
	
	public List<RouterRulesWorktimeItems> findByRouterid(String routerid);
	
	public List<RouterRulesWorktimeItems> findByRouteridAndOrgi(String routerid,String orgi);
}
