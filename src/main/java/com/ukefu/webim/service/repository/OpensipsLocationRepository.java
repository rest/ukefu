package com.ukefu.webim.service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.OpensipsLocation;

public interface OpensipsLocationRepository extends JpaRepository<OpensipsLocation, String> {
	
	public List<OpensipsLocation> findByUsernameAndDomain(String username,String domain);
	
	public List<OpensipsLocation> findByDomain(String domain);
	
	public List<OpensipsLocation> findByUsername(String username);
}
