package com.ukefu.webim.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.StatusEventQueueagent;

public interface StatusEventQueueagentRepository extends JpaRepository<StatusEventQueueagent, String> {
	public StatusEventQueueagent findById(String id);
}
