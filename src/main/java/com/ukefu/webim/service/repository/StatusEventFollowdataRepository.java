package com.ukefu.webim.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.StatusEventFollowData;

public interface StatusEventFollowdataRepository extends JpaRepository<StatusEventFollowData, String> {
	public StatusEventFollowData findById(String id);
}
