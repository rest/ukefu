package com.ukefu.webim.service.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.Tenant;
import com.ukefu.webim.web.model.TenantType;


public abstract interface TenantTypeRepository extends JpaRepository<TenantType, String> {
	
	public abstract TenantType findById(String id);
	
	public abstract TenantType findByName(String name);
	
	public abstract int countByName(String name);

	public Page<TenantType> findAll(Specification<TenantType> spec, Pageable pageable);  //分页按条件查询 
	
	public List<TenantType> findAll(Specification<TenantType> spec);  //按条件查询
	
	public List<TenantType> findByOrderBySortAsc();  //升序

}
