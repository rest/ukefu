package com.ukefu.webim.service.repository;

import com.ukefu.webim.web.model.StatusEventCustom;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusEventCustomRepository extends JpaRepository<StatusEventCustom, String> {

}
