package com.ukefu.webim.service.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.TagSublevel;

public abstract interface TagSublevelRepository  extends JpaRepository<TagSublevel, String>
{
	
	public abstract TagSublevel findByOrgiAndTag(String orgi , String tag);
	
	public abstract int countByOrgiAndTagAndTagtype(String orgi , String tag , String tagtype);
	
	public abstract List<TagSublevel> findByOrgiAndTagAndTagtype(String orgi , String tag , String tagtype);
	
	public abstract TagSublevel findByOrgiAndId(String orgi , String id);
	
	public abstract Page<TagSublevel> findByOrgiAndTagtype(String orgi , String tagtype ,Pageable paramPageable);
	
	public abstract List<TagSublevel> findByOrgi(String orgi);
	
	public abstract List<TagSublevel> findByOrgiAndTagtype(String orgi , String tagtype);
	
	public abstract List<TagSublevel> findByOrgiAndTagtypeOrderBySortindexAsc(String orgi , String tagtype);
	
	public abstract Page<TagSublevel> findByOrgiAndTagid(String orgi , String Tagid ,Pageable paramPageable);
	
	public abstract List<TagSublevel> findByOrgiAndTagidOrderBySortindexAsc(String orgi , String Tagid );
}

