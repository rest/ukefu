package com.ukefu.webim.service.repository;

import org.elasticsearch.search.SearchHit;

public interface InnerDataProcesser {
	public void process(UKResultMapper mapper , Object data , SearchHit hit) ;
}
