package com.ukefu.webim.service.repository;

import com.ukefu.webim.web.model.NumberPool;
import com.ukefu.webim.web.model.NumberPoolExtentionRela;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NumberPoolExtentionRelaRepository extends JpaRepository<NumberPoolExtentionRela, String> {


    public List<NumberPoolExtentionRela> findByHostidAndExtentionid(String hostid,String Extentionid) ;

    public List<NumberPoolExtentionRela> findByExtentionid(String Extentionid) ;

    public Page<NumberPoolExtentionRela> findAll(Specification<NumberPool> spec, Pageable page);

}
