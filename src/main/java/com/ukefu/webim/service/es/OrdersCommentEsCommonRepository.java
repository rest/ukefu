package com.ukefu.webim.service.es;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.ukefu.webim.web.model.OrdersComment;

public interface OrdersCommentEsCommonRepository {
	
	public Page<OrdersComment> findByQuery(BoolQueryBuilder boolQueryBuilder , String q , Pageable page) ;
	
	public Page<OrdersComment> findByQuery(BoolQueryBuilder boolQueryBuilder ,Pageable page) ;
}
