package com.ukefu.webim.service.es;

import static org.elasticsearch.index.query.QueryBuilders.termQuery;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.elasticsearch.index.query.QueryStringQueryBuilder.Operator;
import org.elasticsearch.search.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Component;

import com.ukefu.webim.service.repository.UKResultMapper;
import com.ukefu.webim.web.model.Favorites;

@Component
public class FavoritesRepositoryImpl implements FavoritesEsCommonRepository{
	
	private ElasticsearchTemplate elasticsearchTemplate;

	@Autowired
	public void setElasticsearchTemplate(ElasticsearchTemplate elasticsearchTemplate) {
		this.elasticsearchTemplate = elasticsearchTemplate;
    }

	@Override
	public Page<Favorites> findByQuery(BoolQueryBuilder boolQueryBuilder, String q , Pageable page) {
		if(!StringUtils.isBlank(q)){
	    	boolQueryBuilder.must(new QueryStringQueryBuilder(q).defaultOperator(Operator.AND)) ;
	    }
		NativeSearchQueryBuilder searchQueryBuilder = new NativeSearchQueryBuilder().withQuery(boolQueryBuilder).withSort(new FieldSortBuilder("createtime").unmappedType("date").order(SortOrder.DESC)).withSort(new FieldSortBuilder("name").unmappedType("string").order(SortOrder.DESC));
		
		searchQueryBuilder.withPageable(page) ;
		searchQueryBuilder.withHighlightFields(new HighlightBuilder.Field("title").fragmentSize(200)) ;
		
		Page<Favorites> favoritesList = null ;
		if(elasticsearchTemplate.indexExists(Favorites.class)){
			favoritesList = elasticsearchTemplate.queryForPage(searchQueryBuilder.build() , Favorites.class , new UKResultMapper()) ;
		}
		return favoritesList ;
	}

	@Override
	public List<Favorites> findByOrderidAndOrgi(String orderid ,String orgi) {
		NativeSearchQueryBuilder searchQueryBuilder = new NativeSearchQueryBuilder().withQuery(new BoolQueryBuilder().must(termQuery("orderid" , orderid)).must(termQuery("orgi" , orgi)));
		List<Favorites> favoritesList = null ;
		if(elasticsearchTemplate.indexExists(Favorites.class)){
			favoritesList = elasticsearchTemplate.queryForList(searchQueryBuilder.build() , Favorites.class) ;
		}
		return favoritesList;
	}

	@Override
	public Page<Favorites> findByCreaterAndModel(String creater, String model,
			String q, Pageable page) {
		BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder() ;// TODO Auto-generated method stub
		boolQueryBuilder.must(termQuery("creater" , creater)) ;
		boolQueryBuilder.must(termQuery("model" , model)) ;
		
		NativeSearchQueryBuilder searchQueryBuilder = new NativeSearchQueryBuilder().withQuery(boolQueryBuilder).withSort(new FieldSortBuilder("createtime").unmappedType("date").order(SortOrder.DESC)).withSort(new FieldSortBuilder("name").unmappedType("string").order(SortOrder.DESC));
		
		searchQueryBuilder.withPageable(page) ;
		searchQueryBuilder.withHighlightFields(new HighlightBuilder.Field("title").fragmentSize(200)) ;
		Page<Favorites> favoritesList = null ;
		if(elasticsearchTemplate.indexExists(Favorites.class)){
			favoritesList = elasticsearchTemplate.queryForPage(searchQueryBuilder.build() , Favorites.class) ;
		}
		return favoritesList;
	}
	@Override
	public List<Favorites> findByOrderidAndOrgiAndCreater(String orderid ,String orgi,String creater) {
		NativeSearchQueryBuilder searchQueryBuilder = new NativeSearchQueryBuilder().withQuery(new BoolQueryBuilder().must(termQuery("orderid" , orderid)).must(termQuery("orgi" , orgi)).must(termQuery("creater" , creater)));
		List<Favorites> favoritesList = null ;
		if(elasticsearchTemplate.indexExists(Favorites.class)){
			favoritesList = elasticsearchTemplate.queryForList(searchQueryBuilder.build() , Favorites.class) ;
		}
		return favoritesList;
	}
}
