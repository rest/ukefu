package com.ukefu.webim.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ukefu.util.extra.DataExchangeInterface;
import com.ukefu.webim.service.repository.KnowledgeTypeRepository;
import com.ukefu.webim.web.model.KnowledgeType;

@Service("topictype")
public class TopicTypeDataExchangeImpl implements DataExchangeInterface{
	@Autowired
	private KnowledgeTypeRepository knowledgeTypeRes ;
	
	public String getDataByIdAndOrgi(String id, String orgi){
		KnowledgeType kntype = knowledgeTypeRes.findByIdAndOrgi(id, orgi);
		return  kntype!=null?kntype.getName():id;
	}

	@Override
	public List<KnowledgeType> getListDataByIdAndOrgi(String aiid , String creater, String orgi) {
		return knowledgeTypeRes.findByOrgiAndTypeidOrderBySortindexAsc(orgi , aiid) ;
	}
	
	public void process(Object data , String orgi) {
		
	}
}
