package com.ukefu.webim.service.impl;

import java.util.List;


import org.elasticsearch.index.query.BoolQueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ukefu.util.extra.QualityExchangeInterface;
import com.ukefu.webim.service.es.WorkOrdersRepository;
import com.ukefu.webim.web.model.WorkOrders;

@Service("qcworkorders")
public class QcWorkOrdersDataExchangeImpl implements QualityExchangeInterface{
	@Autowired
	private WorkOrdersRepository workOrdersRes ;
	
	public WorkOrders getDataByIdAndOrgi(String id, String orgi){
		return workOrdersRes.getByIdAndOrgi(id, orgi);
	}

	@Override
	public Page<?> getPageDataByOrgi(Object query, Pageable page) {
		return workOrdersRes.findByOrgi((BoolQueryBuilder) query, page);
	}

	@Override
	public List<?> getListDataByOrgi(Object query) {
		return workOrdersRes.findByOrgiAndQualitydisorgan((BoolQueryBuilder) query);
	}

	@Override
	public void process(Object data) {
		workOrdersRes.save((WorkOrders)data);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void processList(Object data) {
		// TODO Auto-generated method stub
		workOrdersRes.save((List<WorkOrders>)data);
	}
	
	
}
