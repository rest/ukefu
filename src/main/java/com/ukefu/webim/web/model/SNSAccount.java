package com.ukefu.webim.web.model;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.GenericGenerator;

import com.ukefu.util.UKTools;
import com.ukefu.webim.util.OnlineUserUtils;
import com.ukefu.webim.util.server.message.SessionConfigItem;

@Entity
@Table(name = "uk_snsaccount")
@org.hibernate.annotations.Proxy(lazy = false)
public class SNSAccount implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id ;
	private String snsid ;		//表示 SNSAccount
	private String name ;		
	private String code ;		
	private String username ;
	private String password ;
	private String snstype ;//改字段为为大类  微信/微博/易信等
	private Date createtime = new Date();	//创建时间
	private Date updatetime;		//更新时间
	private int expirestime ;		//过期时间
	private String account ;	//该字段修改为子类型   订阅号(sub)/服务号(sev)/企业号(enpt)
	
	private String verify ;		// 是否认证
	private String headimg;
	private String qrcode;	
	private String alias;
	private String openpay;
	private String openshake;
	private String oepnscan;
	private String opencard ;
	private String openstore ;
	private String refreshtoken;
	private String authaccesstoken ;
	
	
	private String allowremote ;
	private String email ;
	private String userno ;
	private String token ;
	private String apipoint ;
	private String appkey ;
	private String secret ;
	private String aeskey ;
	
	
	private String baseURL ;	//网站URL
	
	private String apptoken ;
	private String sessionkey ;
	private boolean defaultaccount ;
	private String moreparam ;				//改变用处，用于记录 爬虫的 爬取位置(微博)/如果是微信记录Secret
	private String orgi ;
	private String lastatupdate ;
	private String lastprimsgupdate ;
	
	private String status = "0";
	private boolean agent = false ;
	
	private String sessionmsg;//人工坐席接入欢迎消息
	private String noagentmsg;//无坐席在线提示消息
	private String agentbusymsg;//坐席忙时提示消息
	private String successmsg;//人工坐席分配成功提示消息
	private String finessmsg;//坐席服务结束提示消息
	private boolean hourcheck;//启用工作时间段设置
	private String workinghours ;	//工作时间段，格式   9:00-12:00,13:30-15:30
	private String notinwhmsg ;		//非工作时间段 访客咨询的提示消息
	private boolean satisfaction;//启用满意度调查
	private String satisftext;//邀请访客评价的提示信息
	private boolean otherquickplay;//启用坐席工作台的外部快捷回复
	private boolean otherssl;//外部机器人启用HTTPS访问
	private String oqrsearchurl;//外部快捷回复的搜索接口地址
	private String oqrsearchinput;//外部快捷回复的搜索输入参数
	private String oqrsearchoutput;//外部快捷回复的搜索输出参数
	private String oqrdetailurl;//外部快捷回复的详情接口地址
	private String oqrdetailinput;//外部快捷回复的详情输入参数
	private String oqrdetailoutput;//外部快捷回复的详情输出参数
	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSnstype() {
		return snstype;
	}
	public void setSnstype(String snstype) {
		this.snstype = snstype;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	
	public String getAllowremote() {
		return allowremote;
	}
	public void setAllowremote(String allowremote) {
		this.allowremote = allowremote;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUserno() {
		return userno;
	}
	public void setUserno(String userno) {
		this.userno = userno;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getAppkey() {
		return appkey;
	}
	public void setAppkey(String appkey) {
		this.appkey = appkey;
	}
	public String getApptoken() {
		return apptoken;
	}
	public void setApptoken(String apptoken) {
		this.apptoken = apptoken;
	}
	public String getSessionkey() {
		return sessionkey;
	}
	public void setSessionkey(String sessionkey) {
		this.sessionkey = sessionkey;
	}
	public String getMoreparam() {
		return moreparam;
	}
	public void setMoreparam(String moreparam) {
		this.moreparam = moreparam;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public String getApipoint() {
		return apipoint;
	}
	public void setApipoint(String apipoint) {
		this.apipoint = apipoint;
	}
	public boolean isDefaultaccount() {
		return defaultaccount;
	}
	public void setDefaultaccount(boolean defaultaccount) {
		this.defaultaccount = defaultaccount;
	}
	public String getLastatupdate() {
		return lastatupdate;
	}
	public void setLastatupdate(String lastatupdate) {
		this.lastatupdate = lastatupdate;
	}
	public String getLastprimsgupdate() {
		return lastprimsgupdate;
	}
	public void setLastprimsgupdate(String lastprimsgupdate) {
		this.lastprimsgupdate = lastprimsgupdate;
	}
	public boolean isAgent() {
		return agent;
	}
	public void setAgent(boolean agent) {
		this.agent = agent;
	}
	@Transient
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getVerify() {
		return verify;
	}
	public void setVerify(String verify) {
		this.verify = verify;
	}
	public String getHeadimg() {
		return headimg;
	}
	public void setHeadimg(String headimg) {
		this.headimg = headimg;
	}
	public String getQrcode() {
		return qrcode;
	}
	public void setQrcode(String qrcode) {
		this.qrcode = qrcode;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public String getOpenpay() {
		return openpay;
	}
	public void setOpenpay(String openpay) {
		this.openpay = openpay;
	}
	public String getOpenshake() {
		return openshake;
	}
	public void setOpenshake(String openshake) {
		this.openshake = openshake;
	}
	public String getOpencard() {
		return opencard;
	}
	public void setOpencard(String opencard) {
		this.opencard = opencard;
	}
	public String getOpenstore() {
		return openstore;
	}
	public void setOpenstore(String openstore) {
		this.openstore = openstore;
	}
	public String getOepnscan() {
		return oepnscan;
	}
	public void setOepnscan(String oepnscan) {
		this.oepnscan = oepnscan;
	}
	public String getRefreshtoken() {
		return refreshtoken;
	}
	public void setRefreshtoken(String refreshtoken) {
		this.refreshtoken = refreshtoken;
	}
	public String getAuthaccesstoken() {
		return authaccesstoken;
	}
	public void setAuthaccesstoken(String authaccesstoken) {
		this.authaccesstoken = authaccesstoken;
	}
	public Date getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}
	public int getExpirestime() {
		return expirestime;
	}
	public void setExpirestime(int expirestime) {
		this.expirestime = expirestime;
	}
	public String toString(){
		return this.id ;
	}
	public String getSecret() {
		return secret;
	}
	public void setSecret(String secret) {
		this.secret = secret;
	}
	public String getAeskey() {
		return aeskey;
	}
	public void setAeskey(String aeskey) {
		this.aeskey = aeskey;
	}
	public String getSnsid() {
		return snsid;
	}
	public void setSnsid(String snsid) {
		this.snsid = snsid;
	}
	public String getBaseURL() {
		return baseURL;
	}
	public void setBaseURL(String baseURL) {
		this.baseURL = baseURL;
	}
	public String getSessionmsg() {
		return sessionmsg;
	}
	public void setSessionmsg(String sessionmsg) {
		this.sessionmsg = sessionmsg;
	}
	public String getNoagentmsg() {
		return noagentmsg;
	}
	public void setNoagentmsg(String noagentmsg) {
		this.noagentmsg = noagentmsg;
	}
	public String getAgentbusymsg() {
		return agentbusymsg;
	}
	public void setAgentbusymsg(String agentbusymsg) {
		this.agentbusymsg = agentbusymsg;
	}
	public String getSuccessmsg() {
		return successmsg;
	}
	public void setSuccessmsg(String successmsg) {
		this.successmsg = successmsg;
	}
	public String getFinessmsg() {
		return finessmsg;
	}
	public void setFinessmsg(String finessmsg) {
		this.finessmsg = finessmsg;
	}
	public boolean isHourcheck() {
		return hourcheck;
	}
	public void setHourcheck(boolean hourcheck) {
		this.hourcheck = hourcheck;
	}
	public String getWorkinghours() {
		return workinghours;
	}
	public void setWorkinghours(String workinghours) {
		this.workinghours = workinghours;
	}
	public String getNotinwhmsg() {
		return notinwhmsg;
	}
	public void setNotinwhmsg(String notinwhmsg) {
		this.notinwhmsg = notinwhmsg;
	}
	public boolean isSatisfaction() {
		return satisfaction;
	}
	public void setSatisfaction(boolean satisfaction) {
		this.satisfaction = satisfaction;
	}
	public String getSatisftext() {
		return satisftext;
	}
	public void setSatisftext(String satisftext) {
		this.satisftext = satisftext;
	}
	public boolean isOtherquickplay() {
		return otherquickplay;
	}
	public void setOtherquickplay(boolean otherquickplay) {
		this.otherquickplay = otherquickplay;
	}
	public boolean isOtherssl() {
		return otherssl;
	}
	public void setOtherssl(boolean otherssl) {
		this.otherssl = otherssl;
	}
	public String getOqrsearchurl() {
		return oqrsearchurl;
	}
	public void setOqrsearchurl(String oqrsearchurl) {
		this.oqrsearchurl = oqrsearchurl;
	}
	public String getOqrsearchinput() {
		return oqrsearchinput;
	}
	public void setOqrsearchinput(String oqrsearchinput) {
		this.oqrsearchinput = oqrsearchinput;
	}
	public String getOqrsearchoutput() {
		return oqrsearchoutput;
	}
	public void setOqrsearchoutput(String oqrsearchoutput) {
		this.oqrsearchoutput = oqrsearchoutput;
	}
	public String getOqrdetailurl() {
		return oqrdetailurl;
	}
	public void setOqrdetailurl(String oqrdetailurl) {
		this.oqrdetailurl = oqrdetailurl;
	}
	public String getOqrdetailinput() {
		return oqrdetailinput;
	}
	public void setOqrdetailinput(String oqrdetailinput) {
		this.oqrdetailinput = oqrdetailinput;
	}
	public String getOqrdetailoutput() {
		return oqrdetailoutput;
	}
	public void setOqrdetailoutput(String oqrdetailoutput) {
		this.oqrdetailoutput = oqrdetailoutput;
	}
	@Transient
	public List<SessionConfigItem> getConfig(){
		List<SessionConfigItem> sessionConfigItemList = null ;
		if(!StringUtils.isBlank(this.getWorkinghours())) {
			try {
				sessionConfigItemList = OnlineUserUtils.objectMapper.readValue(this.getWorkinghours(), UKTools.getCollectionType(ArrayList.class, SessionConfigItem.class))  ;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return sessionConfigItemList ;
	}
	
}
