package com.ukefu.webim.web.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "uk_callcenter_router_worktimeitems")
@org.hibernate.annotations.Proxy(lazy = false)
public class RouterRulesWorktimeItems implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5701358638623529878L;
	
	private String id;
	private String routerid;
	private String worktype ;//work：工作；nowork：非工作
	private int beginhours ;//开始小时
	private int beginmins ;//开始分钟
	private int endhours ;//结束小时
	private int endmins ;//结束分钟
	private String action ;//nowrk时的动作：hangup挂断，trans转接，voice语音播报
	private String num ;//转接号码
	private String voice ;//语音id
	private String orgi;
	
	private String datetype;//日期类型
	
	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRouterid() {
		return routerid;
	}
	public void setRouterid(String routerid) {
		this.routerid = routerid;
	}
	public String getWorktype() {
		return worktype;
	}
	public void setWorktype(String worktype) {
		this.worktype = worktype;
	}
	public int getBeginhours() {
		return beginhours;
	}
	public void setBeginhours(int beginhours) {
		this.beginhours = beginhours;
	}
	public int getBeginmins() {
		return beginmins;
	}
	public void setBeginmins(int beginmins) {
		this.beginmins = beginmins;
	}
	public int getEndhours() {
		return endhours;
	}
	public void setEndhours(int endhours) {
		this.endhours = endhours;
	}
	public int getEndmins() {
		return endmins;
	}
	public void setEndmins(int endmins) {
		this.endmins = endmins;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	public String getVoice() {
		return voice;
	}
	public void setVoice(String voice) {
		this.voice = voice;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public String getDatetype() {
		return datetype;
	}
	public void setDatetype(String datetype) {
		this.datetype = datetype;
	}
	

}
