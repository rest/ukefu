package com.ukefu.webim.web.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
/**
 * 话术-一问多答表
 *
 */
@Entity
@Table(name = "uk_salespatter_multianswer")
@org.hibernate.annotations.Proxy(lazy = false)
public class SalesMultianswer implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -726547850228590446L;
	
	private String id ;
	private String processid ;//所属问卷ID
	private String answerid ;//所属答案ID
	
	private String type ;//答案类型（word文字，voice语音）
	private String word ;//文字
	private String voice ;//语音ID
	
	private String orgi ;		//租户ID
	private String creater ;//创建人

	private Date createtime;
	
	private int sortindex;//排序
	
	private boolean batinterrupt ; //是否支持打断
	private String 	batjump;		//跳转
	private String  bathanguptype;	//提示语类型
	private String  bathangupmsg;	//提示语文字
	private String  bathangupvoice;	//提示语语音ID
	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getProcessid() {
		return processid;
	}
	public void setProcessid(String processid) {
		this.processid = processid;
	}
	public String getAnswerid() {
		return answerid;
	}
	public void setAnswerid(String answerid) {
		this.answerid = answerid;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public String getVoice() {
		return voice;
	}
	public void setVoice(String voice) {
		this.voice = voice;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public String getCreater() {
		return creater;
	}
	public void setCreater(String creater) {
		this.creater = creater;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public int getSortindex() {
		return sortindex;
	}
	public void setSortindex(int sortindex) {
		this.sortindex = sortindex;
	}
	public boolean isBatinterrupt() {
		return batinterrupt;
	}
	public void setBatinterrupt(boolean batinterrupt) {
		this.batinterrupt = batinterrupt;
	}
	public String getBatjump() {
		return batjump;
	}
	public void setBatjump(String batjump) {
		this.batjump = batjump;
	}
	public String getBathanguptype() {
		return bathanguptype;
	}
	public void setBathanguptype(String bathanguptype) {
		this.bathanguptype = bathanguptype;
	}
	public String getBathangupmsg() {
		return bathangupmsg;
	}
	public void setBathangupmsg(String bathangupmsg) {
		this.bathangupmsg = bathangupmsg;
	}
	public String getBathangupvoice() {
		return bathangupvoice;
	}
	public void setBathangupvoice(String bathangupvoice) {
		this.bathangupvoice = bathangupvoice;
	}
	

	
	
}
