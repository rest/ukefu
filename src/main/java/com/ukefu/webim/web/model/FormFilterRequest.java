package com.ukefu.webim.web.model;

public class FormFilterRequest {
	private String[] field;
	private String[] cond;
	private String[] value;
	private String[] comp ;	
	
	private String[] hanguptype ;
	private String[] hangupmsg ;
	private String[] hangupvoice ;
	
	private boolean[] batinterrupt;
	private String[] batjump;
	private String[] bathanguptype;
	private String[] bathangupmsg;
	private String[] bathangupvoice;
	
	public String[] getField() {
		return field;
	}

	public void setField(String[] field) {
		this.field = field;
	}

	public String[] getValue() {
		return value;
	}

	public void setValue(String[] value) {
		this.value = value;
	}

	public String[] getCond() {
		return cond;
	}

	public void setCond(String[] cond) {
		this.cond = cond;
	}

	public String[] getComp() {
		return comp;
	}

	public void setComp(String[] comp) {
		this.comp = comp;
	}

	public String[] getHanguptype() {
		return hanguptype;
	}

	public void setHanguptype(String[] hanguptype) {
		this.hanguptype = hanguptype;
	}

	public String[] getHangupmsg() {
		return hangupmsg;
	}

	public void setHangupmsg(String[] hangupmsg) {
		this.hangupmsg = hangupmsg;
	}

	public String[] getHangupvoice() {
		return hangupvoice;
	}

	public void setHangupvoice(String[] hangupvoice) {
		this.hangupvoice = hangupvoice;
	}



	public boolean[] getBatinterrupt() {
		return batinterrupt;
	}

	public void setBatinterrupt(boolean[] batinterrupt) {
		this.batinterrupt = batinterrupt;
	}

	public String[] getBatjump() {
		return batjump;
	}

	public void setBatjump(String[] batjump) {
		this.batjump = batjump;
	}

	public String[] getBathanguptype() {
		return bathanguptype;
	}

	public void setBathanguptype(String[] bathanguptype) {
		this.bathanguptype = bathanguptype;
	}

	public String[] getBathangupmsg() {
		return bathangupmsg;
	}

	public void setBathangupmsg(String[] bathangupmsg) {
		this.bathangupmsg = bathangupmsg;
	}

	public String[] getBathangupvoice() {
		return bathangupvoice;
	}

	public void setBathangupvoice(String[] bathangupvoice) {
		this.bathangupvoice = bathangupvoice;
	}
	
}
