package com.ukefu.webim.web.model;

import java.io.Serializable;
import java.util.List;

public class ContactsItemExpress implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -244769592802036200L;
	private String status;
	private String distype;//用户、 部门 、释放
	private List<String> targetid ;
	private boolean quality;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDistype() {
		return distype;
	}
	public void setDistype(String distype) {
		this.distype = distype;
	}
	public List<String> getTargetid() {
		return targetid;
	}
	public void setTargetid(List<String> targetid) {
		this.targetid = targetid;
	}
	public boolean isQuality() {
		return quality;
	}
	public void setQuality(boolean quality) {
		this.quality = quality;
	}
	

}
