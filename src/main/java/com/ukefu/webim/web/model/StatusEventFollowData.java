package com.ukefu.webim.web.model;

import com.ukefu.util.event.UserEvent;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "uk_callcenter_event")
@Proxy(lazy = false)
public class StatusEventFollowData implements Serializable,UserEvent{

	private static final long serialVersionUID = 6380455081348698661L;

	private String id ;
	private String quene ;
	private Date updatetime = new Date() ;
	private String accountdes;//客户账号信息
	
	private boolean dtmf ;		//是否记录了DTMF信息
	private String dtmfrec ;

	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}
	public String getAccountdes() {
		return accountdes;
	}
	public void setAccountdes(String accountdes) {
		this.accountdes = accountdes;
	}
	public String getQuene() {
		return quene;
	}
	public void setQuene(String quene) {
		this.quene = quene;
	}
	public String getDtmfrec() {
		return dtmfrec;
	}
	public void setDtmfrec(String dtmfrec) {
		this.dtmfrec = dtmfrec;
	}
	public boolean isDtmf() {
		return dtmf;
	}
	public void setDtmf(boolean dtmf) {
		this.dtmf = dtmf;
	}
}
