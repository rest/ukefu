package com.ukefu.webim.web.model;

import java.util.HashMap;

import com.ukefu.util.UKTools;

public class UKeFuValue<K,V> extends HashMap<K,V>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2110217015030751243L;
	private static UKeFuValue<Object, Object> uKeFuDic = new UKeFuValue<Object, Object>();
	
	public static UKeFuValue<?, ?> getInstance(){
		return uKeFuDic ;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public V get(Object key) {
		return (V) UKTools.md5((String) key);
	}
}
