package com.ukefu.webim.web.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "location")
@org.hibernate.annotations.Proxy(lazy = false)
public class OpensipsLocation implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8628673193465906651L;
	private String contact_id;
	private String username;
	private String domain;
	private String contact ;
	private String received;
	private String path;
	private int expires;
	private BigDecimal q;
	private String callid;
	private int cseq;
	private Date last_modified;
	private int flags;
	private String cflags;
	private String user_agent;
	private String socket;

	private String sip_instance;
	private String kv_store;
	private String attr;
	
	
	
	@Id
	@GeneratedValue
	public String getContact_id() {
		return contact_id;
	}
	public void setContact_id(String contact_id) {
		this.contact_id = contact_id;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getReceived() {
		return received;
	}
	public void setReceived(String received) {
		this.received = received;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public int getExpires() {
		return expires;
	}
	public void setExpires(int expires) {
		this.expires = expires;
	}
	public BigDecimal getQ() {
		return q;
	}
	public void setQ(BigDecimal q) {
		this.q = q;
	}
	public String getCallid() {
		return callid;
	}
	public void setCallid(String callid) {
		this.callid = callid;
	}
	public int getCseq() {
		return cseq;
	}
	public void setCseq(int cseq) {
		this.cseq = cseq;
	}
	public Date getLast_modified() {
		return last_modified;
	}
	public void setLast_modified(Date last_modified) {
		this.last_modified = last_modified;
	}
	public int getFlags() {
		return flags;
	}
	public void setFlags(int flags) {
		this.flags = flags;
	}
	public String getCflags() {
		return cflags;
	}
	public void setCflags(String cflags) {
		this.cflags = cflags;
	}
	public String getUser_agent() {
		return user_agent;
	}
	public void setUser_agent(String user_agent) {
		this.user_agent = user_agent;
	}
	public String getSocket() {
		return socket;
	}
	public void setSocket(String socket) {
		this.socket = socket;
	}

	public String getSip_instance() {
		return sip_instance;
	}
	public void setSip_instance(String sip_instance) {
		this.sip_instance = sip_instance;
	}
	public String getKv_store() {
		return kv_store;
	}
	public void setKv_store(String kv_store) {
		this.kv_store = kv_store;
	}
	public String getAttr() {
		return attr;
	}
	public void setAttr(String attr) {
		this.attr = attr;
	}
	
}
