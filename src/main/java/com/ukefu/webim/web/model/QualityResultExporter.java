package com.ukefu.webim.web.model;

import java.util.List;

public class QualityResultExporter implements java.io.Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3015600072730343541L;
	private QualityResultItem item;//一级
	private List<QualityResultExporter> children ;//下一级
	private int colinx=1;//列数
	
	public QualityResultItem getItem() {
		return item;
	}
	public void setItem(QualityResultItem item) {
		this.item = item;
	}
	public List<QualityResultExporter> getChildren() {
		return children;
	}
	public void setChildren(List<QualityResultExporter> children) {
		this.children = children;
	}
	public int getColinx() {
		return colinx;
	}
	public void setColinx(int colinx) {
		this.colinx = colinx;
	}
	
	
	
	
}
