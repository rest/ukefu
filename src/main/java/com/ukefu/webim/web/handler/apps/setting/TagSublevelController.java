package com.ukefu.webim.web.handler.apps.setting;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ukefu.util.Menu;
import com.ukefu.webim.service.repository.TagRepository;
import com.ukefu.webim.service.repository.TagSublevelRepository;
import com.ukefu.webim.web.handler.Handler;
import com.ukefu.webim.web.model.SysDic;
import com.ukefu.webim.web.model.TagSublevel;
import com.ukefu.webim.web.model.UKeFuDic;

@Controller
@RequestMapping("/setting/tag/sublevel")
public class TagSublevelController extends Handler{
	
	@Autowired
	private TagSublevelRepository tagSublevelRes ;
	
	@Autowired
	private TagRepository tagRes ;
	
	@Value("${web.upload-path}")
    private String path;
	
    @RequestMapping("/tag")
    @Menu(type = "setting" , subtype = "tag" , admin= false)
    public ModelAndView tag(ModelMap map , HttpServletRequest request , @Valid String code, @Valid String tagid) {
    	SysDic tagType = null ;
    	List<SysDic> tagList = UKeFuDic.getInstance().getDic("com.dic.tag.type") ;
    	if(tagList.size() > 0){
    		
    		if(!StringUtils.isBlank(code)){
    			for(SysDic dic : tagList){
    				if(code.equals(dic.getCode())){
    					tagType = dic ;
    				}
    			}
    		}else{
    			tagType = tagList.get(0) ;
    		}
    		map.put("tagType", tagType) ;
    	}
    	if(!StringUtils.isBlank(tagid)){
    		map.put("tagparent", tagRes.findByOrgiAndId(super.getOrgi(request), tagid));
    		map.put("tagList", tagSublevelRes.findByOrgiAndTagid(super.getOrgi(request) , tagid , new PageRequest(super.getP(request), super.getPs(request),Sort.Direction.ASC,new String[]{"sortindex"}))) ;
    	}
    	map.put("tagTypeList", tagList) ;
    	return request(super.createAppsTempletResponse("/apps/setting/agent/tagsublevel/tag"));
    }
    
    @RequestMapping("/add")
    @Menu(type = "setting" , subtype = "tag" , admin= false)
    public ModelAndView tagadd(ModelMap map , HttpServletRequest request , @Valid String tagid, @Valid String tagtype) {
    	map.addAttribute("tagid", tagid) ;
    	map.addAttribute("tagtype", tagtype) ;
        return request(super.createRequestPageTempletResponse("/apps/setting/agent/tagsublevel/tagadd"));
    }
    
    @RequestMapping("/edit")
    @Menu(type = "setting" , subtype = "tag" , admin= false)
    public ModelAndView tagedit(ModelMap map , HttpServletRequest request , @Valid String id , @Valid String tagtype, @Valid String tagid) {
    	map.put("tag", tagSublevelRes.findByOrgiAndId(super.getOrgi(request) , id)) ;
    	map.addAttribute("tagtype", tagtype) ;
    	map.addAttribute("tagid", tagid) ;
        return request(super.createRequestPageTempletResponse("/apps/setting/agent/tagsublevel/tagedit"));
    }
    
    @RequestMapping("/update")
    @Menu(type = "setting" , subtype = "tag" , admin= false)
    public ModelAndView tagupdate(ModelMap map , HttpServletRequest request , @Valid TagSublevel tag , @Valid String tagtype, @Valid String tagid) { 
    	TagSublevel label = tagSublevelRes.findByOrgiAndId(super.getOrgi(request), tag.getId());
    	if(label!=null) {
    		label.setTag(tag.getTag());
    		label.setSortindex(tag.getSortindex());
    		label.setIcon(tag.getIcon());
    		label.setColor(tag.getColor());
	    	tagSublevelRes.save(label) ;
    	}
    	return request(super.createRequestPageTempletResponse("redirect:/setting/tag/sublevel/tag.html?code="+tagtype+"&tagid="+tagid));
    }
    
    @RequestMapping("/save")
    @Menu(type = "setting" , subtype = "tag" , admin= false)
    public ModelAndView tagsave(ModelMap map , HttpServletRequest request , @Valid TagSublevel tag , @Valid String tagtype, @Valid String tagid) {
    	if(tagSublevelRes.countByOrgiAndTagAndTagtype(super.getOrgi(request), tag.getTag() , tagtype) == 0){
    		tag.setParentid("0");
	    	tag.setOrgi(super.getOrgi(request));
	    	tag.setCreater(super.getUser(request).getId());
	    	tagSublevelRes.save(tag) ;
    	}
    	return request(super.createRequestPageTempletResponse("redirect:/setting/tag/sublevel/tag.html?code="+tagtype+"&tagid="+tagid));
    }
    
    @RequestMapping("/delete")
    @Menu(type = "setting" , subtype = "tag" , admin= false)
    public ModelAndView tagdelete(ModelMap map , HttpServletRequest request , @Valid String id , @Valid String tagtype, @Valid String tagid) {
    	//删除操作要先通过orgi查出 在删除
    	TagSublevel tag = tagSublevelRes.findByOrgiAndId(super.getOrgi(request) , id);
		if(tag != null){
			tagSublevelRes.delete(tag);
		}

		return request(super.createRequestPageTempletResponse("redirect:/setting/tag/sublevel/tag.html?code="+tagtype+"&tagid="+tagid));
    }
    
    
    @RequestMapping("/list")
    @Menu(type = "setting" , subtype = "tag")
    public ModelAndView list(ModelMap map , HttpServletRequest request , @Valid String tagid) {
    	if (!StringUtils.isBlank(tagid)) {
    		map.put("tagList", tagSublevelRes.findByOrgiAndTagidOrderBySortindexAsc(super.getOrgi(request), tagid)) ;
		}
		return request(super.createRequestPageTempletResponse("/apps/setting/agent/tagsublevel/list"));
    }

}