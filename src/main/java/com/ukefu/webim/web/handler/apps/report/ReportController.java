package com.ukefu.webim.web.handler.apps.report;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.ukefu.core.UKDataContext;
import com.ukefu.util.Menu;
import com.ukefu.util.ReportExpdata;
import com.ukefu.util.UKTools;
import com.ukefu.webim.service.repository.ColumnPropertiesRepository;
import com.ukefu.webim.service.repository.DataDicRepository;
import com.ukefu.webim.service.repository.OrganRepository;
import com.ukefu.webim.service.repository.PublishedReportRepository;
import com.ukefu.webim.service.repository.ReportAuthRepository;
import com.ukefu.webim.service.repository.ReportCubeService;
import com.ukefu.webim.service.repository.ReportFilterRepository;
import com.ukefu.webim.service.repository.ReportModelRepository;
import com.ukefu.webim.service.repository.ReportRepository;
import com.ukefu.webim.service.repository.RoleRepository;
import com.ukefu.webim.service.repository.UserRepository;
import com.ukefu.webim.web.handler.Handler;
import com.ukefu.webim.web.model.ColumnProperties;
import com.ukefu.webim.web.model.Cube;
import com.ukefu.webim.web.model.CubeMeasure;
import com.ukefu.webim.web.model.CubeMetadata;
import com.ukefu.webim.web.model.DataDic;
import com.ukefu.webim.web.model.Dimension;
import com.ukefu.webim.web.model.Organ;
import com.ukefu.webim.web.model.PublishedCube;
import com.ukefu.webim.web.model.PublishedReport;
import com.ukefu.webim.web.model.Report;
import com.ukefu.webim.web.model.ReportAuth;
import com.ukefu.webim.web.model.ReportFilter;
import com.ukefu.webim.web.model.ReportModel;

@Controller
@RequestMapping("/apps/report")
public class ReportController extends Handler{
	
	@Value("${web.upload-path}")
    private String path;
	
	@Value("${uk.im.server.port}")  
    private Integer port; 
	
	@Autowired
	private DataDicRepository dataDicRes;
	
	@Autowired
	private ReportRepository reportRes;
	
	@Autowired
	private PublishedReportRepository publishedReportRes;
	
	@Autowired
	private ReportFilterRepository reportFilterRepository;
	
	@Autowired
	private ColumnPropertiesRepository columnPropertiesRepository;
	
	@Autowired
	private ReportModelRepository reportModelRes;
	
	@Autowired
	private ReportCubeService reportCubeService;
	
	@Autowired
	private OrganRepository organRes;
	
	@Autowired
	private RoleRepository roleRes;
	
	@Autowired
	private UserRepository userRes;
	
	@Autowired
	private ReportAuthRepository reportAuthRes;
	
    @RequestMapping("/index")
    @Menu(type = "setting" , subtype = "report" )
    public ModelAndView index(ModelMap map , HttpServletRequest request , @Valid String dicid) {
    	if(!StringUtils.isBlank(dicid) && !"0".equals(dicid)){
        	map.put("dataDic", dataDicRes.findByIdAndOrgi(dicid, super.getOrgi(request))) ;
    		map.put("reportList", reportRes.findByOrgiAndDicid(super.getOrgi(request) , dicid , new PageRequest(super.getP(request), super.getPs(request)))) ;
    	}else{
    		map.put("reportList", reportRes.findByOrgi(super.getOrgi(request) , new PageRequest(super.getP(request), super.getPs(request)))) ;
    	}
    	map.put("dataDicList", dataDicRes.findByOrgi(super.getOrgi(request))) ;
    	return request(super.createAppsTempletResponse("/apps/business/report/index"));
    }
    
    @RequestMapping("/add")
    @Menu(type = "setting" , subtype = "reportadd" )
    public ModelAndView quickreplyadd(ModelMap map , HttpServletRequest request , @Valid String dicid) {
    	if(!StringUtils.isBlank(dicid)){
    		map.addAttribute("dataDic", dataDicRes.findByIdAndOrgi(dicid, super.getOrgi(request))) ;
    	}
    	map.addAttribute("dataDicList", dataDicRes.findByOrgi(super.getOrgi(request))) ;
        return request(super.createRequestPageTempletResponse("/apps/business/report/add"));
    }
    
    @RequestMapping("/save")
    @Menu(type = "setting" , subtype = "report" )
    public ModelAndView quickreplysave(ModelMap map , HttpServletRequest request , @Valid Report report) {
    	ModelAndView view = request(super.createRequestPageTempletResponse("redirect:/apps/report/index.html?dicid="+report.getDicid()));
    	if(!StringUtils.isBlank(report.getName())){
    		int count = reportRes.countByOrgiAndName(super.getOrgi(request), report.getName()) ;
    		if(count == 0) {
		    	report.setOrgi(super.getOrgi(request));
				report.setCreater(super.getUser(request).getId());
				report.setReporttype(UKDataContext.ReportType.REPORT.toString());
				report.setCode(UKTools.genID());
				reportRes.save(report) ;
    		}else {
    			view = request(super.createRequestPageTempletResponse("redirect:/apps/report/index.html?msg=rt_name_exist&dicid="+report.getDicid()));
    		}
    	}
        return view ;
    }
    
    @RequestMapping("/delete")
    @Menu(type = "setting" , subtype = "report" )
    public ModelAndView quickreplydelete(ModelMap map , HttpServletRequest request , @Valid String id) {
    	Report report = reportRes.findOne(id) ;
    	if(report!=null){
    		reportRes.delete(report);
    	}
    	return request(super.createRequestPageTempletResponse("redirect:/apps/report/index.html?dicid="+report.getDicid()));
    }
    @RequestMapping("/edit")
    @Menu(type = "setting" , subtype = "report" )
    public ModelAndView quickreplyedit(ModelMap map , HttpServletRequest request , @Valid String id) {
    	Report report = reportRes.findOne(id) ; 
    	map.put("report", report) ;
    	if(report!=null){
    		map.put("dataDic", dataDicRes.findByIdAndOrgi(report.getDicid(), super.getOrgi(request))) ;
    	}
    	map.addAttribute("dataDicList", dataDicRes.findByOrgi(super.getOrgi(request))) ;
        return request(super.createRequestPageTempletResponse("/apps/business/report/edit"));
    }
    
    @RequestMapping("/update")
    @Menu(type = "setting" , subtype = "report" )
    public ModelAndView quickreplyupdate(ModelMap map , HttpServletRequest request , @Valid Report report) {
    	if(!StringUtils.isBlank(report.getId())){
    		Report temp = reportRes.findOne(report.getId()) ;
    		if(temp!=null) {
	    		temp.setName(report.getName());
	    		temp.setCode(report.getCode());
	    		temp.setDicid(report.getDicid());
	    		temp.setUpdatetime(new Date());
	    		temp.setDescription(report.getDescription());
	    		reportRes.save(temp) ;
    		}
    	}
    	return request(super.createRequestPageTempletResponse("redirect:/apps/report/index.html?dicid="+report.getDicid()));
    }
    
    @RequestMapping({"/addtype"})
	@Menu(type="apps", subtype="kbs")
	public ModelAndView addtype(ModelMap map , HttpServletRequest request , @Valid String dicid){
		map.addAttribute("dataDicList", dataDicRes.findByOrgi(super.getOrgi(request))) ;
		if(!StringUtils.isBlank(dicid)){
			map.addAttribute("dataDic", dataDicRes.findByIdAndOrgi(dicid, super.getOrgi(request))) ;
		}
		return request(super.createRequestPageTempletResponse("/apps/business/report/addtype"));
	}

    @RequestMapping("/type/save")
    @Menu(type = "apps" , subtype = "report")
    public ModelAndView typesave(HttpServletRequest request ,@Valid DataDic dataDic) {
    	List<DataDic> dicList = dataDicRes.findByOrgiAndName(super.getOrgi(request),dataDic.getName()) ;
    	if(dicList!=null && dicList.size() > 0){
    		return request(super.createRequestPageTempletResponse("redirect:/apps/report/index.html?dicid="+dataDic.getParentid()+"&msg=qr_type_exist"));
    	}else {
    		dataDic.setOrgi(super.getOrgi(request));
    		dataDic.setCreater(super.getUser(request).getId());
    		dataDic.setCreatetime(new Date());
    		dataDic.setTabtype(UKDataContext.QuickTypeEnum.PUB.toString());
    		dataDicRes.save(dataDic) ;
    	}
    	return request(super.createRequestPageTempletResponse("redirect:/apps/report/index.html?dicid="+dataDic.getId()));
    }
    
    @RequestMapping({"/edittype"})
	@Menu(type="apps", subtype="kbs")
	public ModelAndView edittype(ModelMap map , HttpServletRequest request , String id){
    	DataDic dataDic = dataDicRes.findByIdAndOrgi(id, super.getOrgi(request)) ;
    	map.addAttribute("dataDic", dataDic) ;
    	if(dataDic!=null) {
    		map.addAttribute("parentDataDic", dataDicRes.findByIdAndOrgi(dataDic.getParentid(), super.getOrgi(request))) ;
    	}
		map.addAttribute("dataDicList", dataDicRes.findByOrgi(super.getOrgi(request))) ;
		return request(super.createRequestPageTempletResponse("/apps/business/report/edittype"));
	}
	 
    @RequestMapping("/type/update")
    @Menu(type = "apps" , subtype = "report")
    public ModelAndView typeupdate(HttpServletRequest request ,@Valid DataDic dataDic) {
    	ModelAndView view = request(super.createRequestPageTempletResponse("redirect:/apps/report/index.html?dicid="+dataDic.getId()));
    	DataDic tempDataDic= dataDicRes.findByIdAndOrgi(dataDic.getId(), super.getOrgi(request)) ;
    	if(tempDataDic !=null){
    		//判断名称是否重复
    		List<DataDic> dicList = dataDicRes.findByOrgiAndNameAndIdNot(super.getOrgi(request) , dataDic.getName() , dataDic.getId()) ;
    		if(dicList!=null && dicList.size() > 0) {
    			view = request(super.createRequestPageTempletResponse("redirect:/apps/report/index.html?msg=qr_type_exist&dicid="+dataDic.getId()));
    		}else {
    			tempDataDic.setName(dataDic.getName());
    			tempDataDic.setDescription(dataDic.getDescription());
    			tempDataDic.setParentid(dataDic.getParentid());
	    		dataDicRes.save(tempDataDic) ;
    		}
    	}
    	return view ;
    }
    
    @RequestMapping({"/deletetype"})
	@Menu(type="apps", subtype="kbs")
	public ModelAndView deletetype(ModelMap map , HttpServletRequest request , @Valid String id){
    	ModelAndView view = request(super.createRequestPageTempletResponse("redirect:/apps/report/index.html?dicid="+id)); 
    	if(!StringUtils.isBlank(id)){
    		DataDic tempDataDic = dataDicRes.findByIdAndOrgi(id, super.getOrgi(request)) ;
    		int count = reportRes.countByOrgiAndDicid(super.getOrgi(request), id) ;
    		if(count == 0) {
    			dataDicRes.delete(tempDataDic);
    			view = request(super.createRequestPageTempletResponse("redirect:/apps/report/index.html?dicid="+tempDataDic.getParentid())); 
    		}else {
    			view = request(super.createRequestPageTempletResponse("redirect:/apps/report/index.html?msg=report_exist&dicid="+id)); 
    		}
    	}
    	return view ;
	}
    
    @RequestMapping("/imp")
    @Menu(type = "setting" , subtype = "reportimp")
    public ModelAndView imp(ModelMap map , HttpServletRequest request , @Valid String dicid) {
    	map.addAttribute("dicid", dicid) ;
        return request(super.createRequestPageTempletResponse("/apps/business/report/imp"));
    }
    
    @SuppressWarnings("unchecked")
    @RequestMapping("/impsave")
    @Menu(type = "setting" , subtype = "reportimpsave")
    public ModelAndView impsave(ModelMap map , HttpServletRequest request , @Valid String dicid,@RequestParam(value = "dataFile", required = false) MultipartFile dataFile) throws IOException {
    	if(dataFile!=null && dataFile.getSize() > 0){
			try {
				List<ReportExpdata> reportExpdatas = (List<ReportExpdata>) UKTools.toObject(dataFile.getBytes());
				if(reportExpdatas!=null && reportExpdatas.size() > 0){
					for(ReportExpdata reportExpdata : reportExpdatas) {
						if(reportExpdata.getReport()!=null && reportRes.findByIdAndOrgi(reportExpdata.getReport().getId() , reportExpdata.getReport().getOrgi()) == null) {
							reportExpdata.getReport().setDicid(dicid);;
							reportRes.save(reportExpdata.getReport()) ;
							reportModelRes.save(reportExpdata.getReportModels());
							reportFilterRepository.save(reportExpdata.getReportFilters()) ;
							columnPropertiesRepository.save(reportExpdata.getColumnProperties()) ;
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return request(super.createRequestPageTempletResponse("redirect:/apps/report/index.html"+(!StringUtils.isBlank(dicid)? "?dicid="+dicid:"")));
    }
    
    @RequestMapping("/batdelete")
    @Menu(type = "setting" , subtype = "reportbatdelete")
    public ModelAndView batdelete(ModelMap map , HttpServletRequest request , HttpServletResponse response , @Valid String[] ids ,@Valid String type) throws IOException {
    	if(ids!=null && ids.length > 0){
    		Iterable<Report> topicList = reportRes.findAll(Arrays.asList(ids)) ;
    		reportRes.delete(topicList);
    	}
    	
    	return request(super.createRequestPageTempletResponse("redirect:/apps/report/index.html"+(!StringUtils.isBlank(type) ? "?dicid="+type:"")));
    }
    
    @RequestMapping("/expids")
    @Menu(type = "setting" , subtype = "reportexpids")
    public void expids(ModelMap map , HttpServletRequest request , HttpServletResponse response , @Valid String[] ids) throws Exception {
    	if(ids!=null && ids.length > 0){
    		List<ReportExpdata> expdatas = new ArrayList<ReportExpdata>();
    		Iterable<Report> topicList = reportRes.findAll(Arrays.asList(ids)) ;
    		for(Report report : topicList) {
    			ReportExpdata expdata = new ReportExpdata(report, 
    					reportModelRes.findByOrgiAndReportid(super.getOrgi(request), report.getId(), new Sort(Sort.Direction.ASC,"colindex")), 
    					reportFilterRepository.findByReportidAndOrgi(report.getId(), super.getOrgi(request))) ;
    			if(expdata.getReportModels()!=null && expdata.getReportModels().size() > 0) {
    				List<ColumnProperties> columnProperties = new ArrayList<>();
    				for(ReportModel model : expdata.getReportModels()) {
    					columnProperties.addAll(columnPropertiesRepository.findByModelid(model.getId()));
    				}
    				expdata.setColumnProperties(columnProperties);
    			}
    			expdatas.add(expdata) ;
    		}
    		if(expdatas!=null && expdatas.size() > 0){
    			response.setHeader("content-disposition", "attachment;filename=UCKeFu-Report-Export-"+new SimpleDateFormat("yyyy-MM-dd").format(new Date())+".data");
    			response.getOutputStream().write(UKTools.toBytes(expdatas));
    		}
    	}
    	
        return ;
    }
    
    @RequestMapping("/expall")
    @Menu(type = "setting" , subtype = "reportexpall")
    public void expall(ModelMap map , HttpServletRequest request , HttpServletResponse response,@Valid String type) throws Exception {
    	List<Report> reportList = null;
    	if (!StringUtils.isBlank(type)) {
    		reportList = reportRes.findByOrgiAndDicid(super.getOrgi(request) , type) ;
		}else{
			reportList = reportRes.findByOrgi(super.getOrgi(request)) ;
		}
    	if (reportList!=null && reportList.size() > 0) {
    		List<ReportExpdata> expdatas = new ArrayList<ReportExpdata>();
    		for(Report report : reportList) {
    			ReportExpdata expdata = new ReportExpdata(report, 
    					reportModelRes.findByOrgiAndReportid(super.getOrgi(request), report.getId(), new Sort(Sort.Direction.ASC,"colindex")), 
    					reportFilterRepository.findByReportidAndOrgi(report.getId(), super.getOrgi(request))) ;
    			if(expdata.getReportModels()!=null && expdata.getReportModels().size() > 0) {
    				List<ColumnProperties> columnProperties = new ArrayList<>();
    				for(ReportModel model : expdata.getReportModels()) {
    					columnProperties.addAll(columnPropertiesRepository.findByModelid(model.getId()));
    				}
    				expdata.setColumnProperties(columnProperties);
    			}
    			expdatas.add(expdata) ;
    		}
    		if(expdatas!=null && expdatas.size() > 0){
    			response.setHeader("content-disposition", "attachment;filename=UCKeFu-Report-Export-"+new SimpleDateFormat("yyyy-MM-dd").format(new Date())+".data");
    			response.getOutputStream().write(UKTools.toBytes(expdatas));
    		}
		}
        return ;
    }
    
    @RequestMapping("/pbreportindex")
    @Menu(type = "setting" , subtype = "pbreport" )
    public ModelAndView pbreportindex(ModelMap map , HttpServletRequest request , @Valid String dicid) {
    	if(!StringUtils.isBlank(dicid) && !"0".equals(dicid)){
        	map.put("dataDic", dataDicRes.findByIdAndOrgi(dicid, super.getOrgi(request))) ;
    		map.put("reportList", publishedReportRes.findByOrgiAndDicid(super.getOrgi(request) , dicid , new PageRequest(super.getP(request), super.getPs(request)))) ;
    	}else{
    		map.put("reportList", publishedReportRes.findByOrgi(super.getOrgi(request) , new PageRequest(super.getP(request), super.getPs(request)))) ;
    	}
    	map.put("dataDicList", dataDicRes.findByOrgi(super.getOrgi(request))) ;
    	return request(super.createAppsTempletResponse("/apps/business/report/pbreportindex"));
    }
    @RequestMapping("/pbreportlist")
    @Menu(type = "setting" , subtype = "pbreport" )
    public ModelAndView pbreportlist(ModelMap map , HttpServletRequest request , @Valid String dicid) {
    	if(!StringUtils.isBlank(dicid) && !"0".equals(dicid)){
        	map.put("dataDic", dataDicRes.findByIdAndOrgi(dicid, super.getOrgi(request))) ;
    		map.put("reportList", publishedReportRes.findByOrgiAndDicid(super.getOrgi(request) , dicid , new PageRequest(super.getP(request), super.getPs(request)))) ;
    	}else{
    		map.put("reportList", publishedReportRes.findByOrgi(super.getOrgi(request) , new PageRequest(super.getP(request), super.getPs(request)))) ;
    	}
    	map.put("dataDicList", dataDicRes.findByOrgi(super.getOrgi(request))) ;
    	return request(super.createRequestPageTempletResponse("/apps/business/report/pbreportlist"));
    }
    
    @RequestMapping("/pbdelete")
    @Menu(type = "setting" , subtype = "pbreport" )
    public ModelAndView pbdelete(ModelMap map , HttpServletRequest request , @Valid String id) {
    	PublishedReport report = publishedReportRes.findOne(id) ;
    	if(report!=null){
    		publishedReportRes.delete(report);
    	}
    	return request(super.createRequestPageTempletResponse("redirect:/apps/report/pbreportindex.html?dicid="+report.getDicid()));
    }
    
    @RequestMapping("/unpub")
    @Menu(type = "setting" , subtype = "pbreport" )
    public ModelAndView unpub(ModelMap map , HttpServletRequest request , @Valid String id) {
    	PublishedReport pubReport = publishedReportRes.findOne(id) ;
    	if(pubReport!=null){
    		Report report = pubReport.getReport() ;
    		List<ReportFilter> filters = report.getReportFilters() ;
    		for(ReportFilter filter : filters) {
    			if(reportFilterRepository.findByIdAndOrgi(filter.getId() ,super.getOrgi(request))==null){
    				reportFilterRepository.save(filter) ;
    			}
    		}
    		for(ReportModel model : report.getReportModels()) {
    			for(ColumnProperties col : model.getColproperties()) {
    				columnPropertiesRepository.save(col) ;
    			}
    			for(ColumnProperties pro : model.getProperties()) {
    				columnPropertiesRepository.save(pro) ;
    			}
    			for(ColumnProperties measure : model.getMeasures()) {
    				columnPropertiesRepository.save(measure) ;
    			}
    			for(ReportFilter filter : model.getFilters()) {
    				reportFilterRepository.save(filter) ;
    			}
    			if(reportModelRes.findByIdAndOrgi(model.getId() ,super.getOrgi(request)) == null) {
    				reportModelRes.save(model) ;
    			}
    		}
    		if(reportRes.findByIdAndOrgi(report.getId(), super.getOrgi(request)) == null) {
    			reportRes.save(report) ;
    		}
    	}
    	return request(super.createRequestPageTempletResponse("redirect:/apps/report/pbreportindex.html?dicid="+pubReport.getDicid()));
    }
    /**
     * 报表
     * @param map
     * @param request
     * @param id
     * @return
     * @throws Exception 
     */
    @RequestMapping("/view")
	@Menu(type = "report", subtype = "report")
	public ModelAndView view(ModelMap map, HttpServletRequest request, @Valid String id) throws Exception {
		if (!StringUtils.isBlank(id)) {
			PublishedReport publishedReport = publishedReportRes.findById(id);
			if(publishedReport!=null) {
				map.addAttribute("publishedReport", publishedReport);
				map.addAttribute("report", publishedReport.getReport());
				map.addAttribute("reportModels", publishedReport.getReport().getReportModels());
				List<ReportFilter> listFilters = publishedReport.getReport().getReportFilters();
				if(!listFilters.isEmpty()) {
					Map<String,ReportFilter> filterMap = new HashMap<String,ReportFilter>();
					for(ReportFilter rf:listFilters) {
						filterMap.put(rf.getId(), rf);
					}
					for(ReportFilter rf:listFilters) {
						if(!StringUtils.isBlank(rf.getCascadeid())) {
							rf.setChildFilter(filterMap.get(rf.getCascadeid()));
						}
					}
				}
				map.addAttribute("reportFilters", reportCubeService.fillReportFilterData(listFilters, request));
			}
		}
		return request(super.createRequestPageTempletResponse("/apps/business/report/view"));
	}


	/**
	 * 选择已发布报表列表 （系统级报表 orgi为ukewo）
	 * @param map
	 * @param request
	 * @param typeid
	 * @return
	 */
	@RequestMapping("/selpbreportindex")
	@Menu(type = "setting" , subtype = "pbreport" )
	public ModelAndView selpbreportindex(ModelMap map , HttpServletRequest request, @Valid String typeid, @Valid String mid) {

		//分类
		String orgi = UKDataContext.SYSTEM_ORGI;

		map.put("dataDicList", dataDicRes.findByOrgi(orgi)) ;

		if(!StringUtils.isBlank(typeid)){
			map.put("reportList", publishedReportRes.findByOrgiAndDicid(orgi ,typeid, new PageRequest(super.getP(request), super.getPs(request)))) ;
			map.put("dataDic", dataDicRes.findByIdAndOrgi(typeid, orgi)) ;
		}else{
			map.put("reportList", publishedReportRes.findByOrgi(orgi , new PageRequest(super.getP(request), super.getPs(request)))) ;
		}

		map.put("typeid", typeid);
		map.put("mid", mid);
		return request(super.createRequestPageTempletResponse("/apps/business/report/pbreport/pbreportindex"));
	}
	/**
	 * 选择已发布模型列表
	 * @param map
	 * @param request
	 * @param typeid
	 * @return
	 */
	@RequestMapping("/selpbreportlist")
	@Menu(type = "setting" , subtype = "pbreport" )
	public ModelAndView selpbcubelist(ModelMap map , HttpServletRequest request , @Valid String typeid, @Valid String mid) {
		String orgi = UKDataContext.SYSTEM_ORGI;

		map.put("dataDicList", dataDicRes.findByOrgi(orgi)) ;

		if(!StringUtils.isBlank(typeid)){
			map.put("reportList", publishedReportRes.findByOrgiAndDicid(orgi ,typeid, new PageRequest(super.getP(request), super.getPs(request)))) ;
			map.put("dataDic", dataDicRes.findByIdAndOrgi(typeid, orgi)) ;
		}else{
			map.put("reportList", publishedReportRes.findByOrgi(orgi , new PageRequest(super.getP(request), super.getPs(request)))) ;
		}

		map.put("typeid", typeid);
		map.put("mid", mid);
		return request(super.createRequestPageTempletResponse("/apps/business/report/pbreport/pbreportlist"));
	}

	@RequestMapping("/reference")
	@Menu(type = "setting" , subtype = "pbreport" )
	public ModelAndView reference(ModelMap map , HttpServletRequest request,@Valid String reportid,@Valid String typeid ) throws Exception {
		String orgi = super.getOrgi(request);

		PublishedReport publishedReport = publishedReportRes.findByIdAndOrgi(reportid,UKDataContext.SYSTEM_ORGI);

		if(publishedReport != null){
			PublishedReport publishedReport2 = new PublishedReport();
			UKTools.copyProperties(publishedReport,publishedReport2,"id");

			Base64 base64 = new Base64();
			Report report = publishedReport2.getReport();
			if(report != null){
				List<ReportModel> reportModels = report.getReportModels();
				if(reportModels != null && reportModels.size() > 0){
					for(ReportModel rm : reportModels){
						PublishedCube pc = rm.getPublishedCube();
						if(pc != null){
							pc.setOrgi(orgi);
							Cube cube = pc.getCube();
							if(cube != null){
								cube.setOrgi(orgi);
								List<CubeMetadata> metadata = cube.getMetadata();
								List<Dimension> dimension = cube.getDimension();
								List<CubeMeasure> measure = cube.getMeasure();
								if(metadata != null && metadata.size() > 0){
									for(CubeMetadata d : metadata){
										d.setOrgi(orgi);
									}
								}
								if(dimension != null && dimension.size() > 0){
									for(Dimension d : dimension){
										d.setOrgi(orgi);
									}
								}
								if(measure != null && measure.size() > 0){
									for(CubeMeasure d : measure){
										d.setOrgi(orgi);
									}
								}
								cube.setMetadata(metadata);
								cube.setDimension(dimension);
								cube.setMeasure(measure);
								pc.setCubecontent(base64.encodeToString(UKTools.toBytes(cube)));
							}
							rm.setPublishedCube(pc);
						}
					}
					report.setReportModels(reportModels);
				}
				List<ReportFilter> reportFilters = report.getReportFilters();
				if(reportFilters != null && reportFilters.size() > 0){
					for(ReportFilter rm : reportFilters){
						rm.setOrgi(orgi);
					}
					report.setReportFilters(reportFilters);
				}
				publishedReport2.setReportcontent(base64.encodeToString(UKTools.toBytes(report)));
			}
			publishedReport2.setDicid(typeid);
			publishedReport2.setOrgi(super.getOrgi(request));
			publishedReportRes.save(publishedReport2);
		}
		return request(super.createRequestPageTempletResponse("redirect:/apps/report/pbreportindex.html?dicid="+typeid));
	}
	
	@RequestMapping("/pub/expids")
    @Menu(type = "setting" , subtype = "reportexpids")
    public void pubexpids(ModelMap map , HttpServletRequest request , HttpServletResponse response , @Valid String[] ids) throws Exception {
    	if(ids!=null && ids.length > 0){
    		List<ReportExpdata> expdatas = new ArrayList<ReportExpdata>();
    		Iterable<PublishedReport> topicList = publishedReportRes.findAll(Arrays.asList(ids)) ;
    		for(PublishedReport pubReport : topicList) {
    			Report report = reportRes.findByIdAndOrgi(pubReport.getDataid(), pubReport.getOrgi()) ;
    			if(report!=null) {
	    			ReportExpdata expdata = new ReportExpdata(report, 
	    					reportModelRes.findByOrgiAndReportid(super.getOrgi(request), report.getId(), new Sort(Sort.Direction.ASC,"colindex")), 
	    					reportFilterRepository.findByReportidAndOrgi(report.getId(), super.getOrgi(request))) ;
	    			if(expdata.getReportModels()!=null && expdata.getReportModels().size() > 0) {
	    				List<ColumnProperties> columnProperties = new ArrayList<>();
	    				for(ReportModel model : expdata.getReportModels()) {
	    					columnProperties.addAll(columnPropertiesRepository.findByModelid(model.getId()));
	    				}
	    				expdata.setColumnProperties(columnProperties);
	    			}
	    			expdata.setPubReport(pubReport);
	    			expdatas.add(expdata) ;
    			}
    		}
    		if(expdatas!=null && expdatas.size() > 0){
    			response.setHeader("content-disposition", "attachment;filename=UCKeFu-PubReport-Export-"+new SimpleDateFormat("yyyy-MM-dd").format(new Date())+".data");
    			response.getOutputStream().write(UKTools.toBytes(expdatas));
    		}
    	}
    	
        return ;
    }
	
	@RequestMapping("/pub/imp")
	@Menu(type = "apps" , subtype = "report")
	public ModelAndView pubimp(ModelMap map , HttpServletRequest request , @Valid String dicid) {
    	map.put("dicid", dicid);
		return request(super.createRequestPageTempletResponse("/apps/business/report/pubimp"));
	}

	@SuppressWarnings("unchecked")
	@RequestMapping("/pub/impsave")
	@Menu(type = "apps" , subtype = "report")
	public ModelAndView pubimpsave(ModelMap map , HttpServletRequest request , @Valid String dicid,@RequestParam(value = "dataFile", required = false) MultipartFile dataFile) {
		if(dataFile!=null && dataFile.getSize() > 0){
			try {
				List<ReportExpdata> reportExpdatas = (List<ReportExpdata>) UKTools.toObject(dataFile.getBytes());
				if(reportExpdatas!=null && reportExpdatas.size() > 0){
					for(ReportExpdata reportExpdata : reportExpdatas) {
						if(reportExpdata.getPubReport()!=null && publishedReportRes.findById(reportExpdata.getPubReport().getId()) == null) {
							reportExpdata.getPubReport().setDicid(dicid);
							publishedReportRes.save(reportExpdata.getPubReport()) ;
						}
						if(reportExpdata.getReport()!=null && reportRes.findByIdAndOrgi(reportExpdata.getReport().getId() , reportExpdata.getReport().getOrgi()) == null) {
							reportExpdata.getReport().setDicid(dicid);;
							reportRes.save(reportExpdata.getReport()) ;
							reportModelRes.save(reportExpdata.getReportModels());
							reportFilterRepository.save(reportExpdata.getReportFilters()) ;
							columnPropertiesRepository.save(reportExpdata.getColumnProperties()) ;
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return request(super.createRequestPageTempletResponse("redirect:/apps/report/pbreportindex.html?dicid="+dicid));
	}
	
	@RequestMapping("/auth/edittype")
	@Menu(type = "apps" , subtype = "report")
	public ModelAndView aedittype(ModelMap map , HttpServletRequest request , @Valid String dicid) {
    	map.put("dataDic", dataDicRes.findByIdAndOrgi(dicid, super.getOrgi(request)));
		return request(super.createRequestPageTempletResponse("/apps/business/report/pbreport/auth/edittype"));
	}
	
	@RequestMapping("/auth/edittype/save")
	@Menu(type = "apps" , subtype = "report")
	public ModelAndView aedittypesave(ModelMap map , HttpServletRequest request , @Valid DataDic datadic) {
		DataDic dic = dataDicRes.findByIdAndOrgi(datadic.getId(), super.getOrgi(request));
		if (dic != null ) {
			dic.setUserauth(datadic.isUserauth());
			dataDicRes.save(dic);
		}
		return request(super.createRequestPageTempletResponse("redirect:/apps/report/pbreportindex.html?dicid="+datadic.getId()));
	}
	
	@RequestMapping("/auth")
	@Menu(type = "apps" , subtype = "report")
	public ModelAndView dicauth(ModelMap map , HttpServletRequest request , @Valid String id,@Valid String dicid, @Valid String authtype, @Valid String type) {
    	map.put("id", id);
    	map.put("dicid", dicid);
    	map.put("type", type);
    	map.put("authtype", authtype);
    	ReportAuth reportauth =  null;
    	if (!StringUtils.isBlank(type) && "dataDic".equals(type)) {
    		map.put("reportauth", reportauth = reportAuthRes.findByDicidAndOrgi(id, super.getOrgi(request)));
		}else{
			map.put("reportauth", reportauth = reportAuthRes.findByReportAndOrgi(id, super.getOrgi(request)));
		}
    	if (!StringUtils.isBlank(authtype) && "organ".equals(authtype)) {
    		if (reportauth != null && !StringUtils.isBlank(reportauth.getAuthorgan())) {
    			String[] ids = reportauth.getAuthorgan().split(",");
    			if (ids != null && ids.length>0) {
    				map.put("disorganList", organRes.findByIdInAndOrgi(Arrays.asList(ids), super.getOrgi(request)));
				}
			}
    		if (!StringUtils.isBlank(type) && "dataDic".equals(type)) {
    			map.put("organList", organRes.findByOrgi(super.getOrgi(request)));
    			return request(super.createRequestPageTempletResponse("/apps/business/report/pbreport/auth/organ"));
    		}else{
    			ReportAuth dicAuth = reportAuthRes.findByDicidAndOrgi(dicid, super.getOrgi(request));
    			if (dicAuth != null && !StringUtils.isBlank(dicAuth.getAuthorgan())) {
					String[] oids = dicAuth.getAuthorgan().split(",");
					if (oids != null && oids.length > 0) {
						map.put("organList", organRes.findByIdInAndOrgi(Arrays.asList(oids), super.getOrgi(request)));
					}
				}
    			return request(super.createRequestPageTempletResponse("/apps/business/report/pbreport/auth/rorgan"));
    		}
    		
		}
    	if (!StringUtils.isBlank(authtype) && "role".equals(authtype)) {
    		
    		if (!StringUtils.isBlank(type) && "dataDic".equals(type)) {
    			map.put("roleList", roleRes.findByOrgi(super.getOrgi(request)));
    		}else{
    			ReportAuth dicAuth = reportAuthRes.findByDicidAndOrgi(dicid, super.getOrgi(request));
    			if (dicAuth != null && !StringUtils.isBlank(dicAuth.getAuthrole())) {
					String[] oids = dicAuth.getAuthrole().split(",");
					if (oids != null && oids.length > 0) {
						map.put("roleList", roleRes.findByIdInAndOrgi(Arrays.asList(oids), super.getOrgi(request)));
					}
				}
    		}
    		
    		if (reportauth != null && !StringUtils.isBlank(reportauth.getAuthrole())) {
    			String[] ids = reportauth.getAuthrole().split(",");
    			if (ids != null && ids.length>0) {
    				map.put("disroleList", roleRes.findByIdInAndOrgi(Arrays.asList(ids), super.getOrgi(request)));
				}
			}
    		return request(super.createRequestPageTempletResponse("/apps/business/report/pbreport/auth/role"));
		}
    	if (!StringUtils.isBlank(authtype) && "user".equals(authtype)) {
    		
    		if (!StringUtils.isBlank(type) && "dataDic".equals(type)) {
    			map.put("userList", userRes.findByOrgiAndDatastatus(super.getOrgi(request), false));
    		}else{
    			ReportAuth dicAuth = reportAuthRes.findByDicidAndOrgi(dicid, super.getOrgi(request));
    			if (dicAuth != null && !StringUtils.isBlank(dicAuth.getAuthuser())) {
					String[] oids = dicAuth.getAuthuser().split(",");
					if (oids != null && oids.length > 0) {
						map.put("userList", userRes.findByIdInAndOrgi(Arrays.asList(oids), super.getOrgi(request)));
					}
				}
    		}
    		
    		if (reportauth != null && !StringUtils.isBlank(reportauth.getAuthuser())) {
    			String[] ids = reportauth.getAuthuser().split(",");
    			if (ids != null && ids.length>0) {
    				map.put("disuserList", userRes.findByIdInAndOrgi(Arrays.asList(ids), super.getOrgi(request)));
				}
			}
    		return request(super.createRequestPageTempletResponse("/apps/business/report/pbreport/auth/user"));
		}
		return request(super.createRequestPageTempletResponse("/apps/business/report/pbreport/auth/organ"));
	}
	
	@RequestMapping("/auth/save")
	@Menu(type = "apps" , subtype = "report")
	public ModelAndView dicauthsave(ModelMap map , HttpServletRequest request , @Valid String id, @Valid String type, @Valid String dicid, @Valid String agentno, @Valid String roleid, @Valid String userid) {
		if (!StringUtils.isBlank(id) && !StringUtils.isBlank(type)) {
			ReportAuth reportauth =  null;
			if ("dataDic".equals(type)) {
				reportauth = reportAuthRes.findByDicidAndOrgi(id, super.getOrgi(request));
			}else{
				reportauth = reportAuthRes.findByReportAndOrgi(id, super.getOrgi(request));
			}
			if (reportauth==null) {
				reportauth = new ReportAuth();
			}
			reportauth.setCreater(super.getUser(request).getId());
			reportauth.setCreatetime(new Date());
			reportauth.setOrgi(super.getOrgi(request));
			reportauth.setType(type);
			reportauth.setDatadicid(dicid);
			if ("dataDic".equals(type)) {
				reportauth.setDicid(id);
			}else{
				reportauth.setReport(id);
			}
			if (!StringUtils.isBlank(agentno)) {
				if (!StringUtils.isBlank(reportauth.getAuthorgan())) {
					reportauth.setAuthorgan(reportauth.getAuthorgan()+","+agentno);
				}else{
					reportauth.setAuthorgan(agentno);
				}
			}
			if (!StringUtils.isBlank(roleid)) {
				if (!StringUtils.isBlank(reportauth.getAuthrole())) {
					reportauth.setAuthrole(reportauth.getAuthrole()+","+roleid);
				}else{
					reportauth.setAuthrole(roleid);
				}
			}
			if (!StringUtils.isBlank(userid)) {
				if (!StringUtils.isBlank(reportauth.getAuthuser())) {
					reportauth.setAuthuser(reportauth.getAuthuser()+","+userid);
				}else{
					reportauth.setAuthuser(userid);
				}
			}
			reportAuthRes.save(reportauth);
		}
		
		return request(super.createRequestPageTempletResponse("redirect:/apps/report/pbreportindex.html?dicid="+dicid));
	}
	
	@RequestMapping("/auth/delete")
	@Menu(type = "apps" , subtype = "report")
	public ModelAndView dicauthdelete(ModelMap map , HttpServletRequest request , @Valid String id,@Valid String dicid, @Valid String authtype, @Valid String type, @Valid String delid) {
    	map.put("id", id);
    	map.put("dicid", dicid);
    	map.put("type", type);
    	map.put("authtype", authtype);
    	ReportAuth reportauth =  null;
    	if (!StringUtils.isBlank(type) && "dataDic".equals(type)) {
    		reportauth = reportAuthRes.findByDicidAndOrgi(id, super.getOrgi(request));
		}else{
			reportauth = reportAuthRes.findByReportAndOrgi(id, super.getOrgi(request));
		}
    	if (!StringUtils.isBlank(authtype) && "organ".equals(authtype)) {
    		if (reportauth != null && !StringUtils.isBlank(reportauth.getAuthorgan())) {
    			String[] ids = reportauth.getAuthorgan().split(",");
    			List<String> idList = new ArrayList<>();
    			if (ids != null && ids.length>0 && !StringUtils.isBlank(delid)) {
    				for(String idstr:ids){
    					if (!delid.equals(idstr)) {
    						idList.add(idstr);
						}
    				}
    				reportauth.setAuthorgan(idList.size()>0?idList.toString().replace("[", "").replace("]", "").replaceAll(" ", ""):null);
    				reportAuthRes.save(reportauth);
    				if (idList.size() >0) {
    					map.put("disorganList", organRes.findByIdInAndOrgi(idList, super.getOrgi(request)));
					}
				}
			}
    		map.put("reportauth", reportauth);
    		if (!StringUtils.isBlank(type) && "dataDic".equals(type)) {
    			List<ReportAuth> reportAuthList = reportAuthRes.findByDatadicidAndTypeAndOrgi(reportauth.getDicid(), "report", super.getOrgi(request));
    			if (reportAuthList != null && reportAuthList.size() > 0) {
					for(ReportAuth rauth : reportAuthList){
						String[] ids = rauth.getAuthorgan().split(",");
		    			List<String> idList = new ArrayList<>();
		    			if (ids != null && ids.length>0 && !StringUtils.isBlank(delid)) {
		    				for(String idstr:ids){
		    					if (!delid.equals(idstr)) {
		    						idList.add(idstr);
								}
		    				}
		    				rauth.setAuthorgan(idList.size()>0?idList.toString().replace("[", "").replace("]", "").replaceAll(" ", ""):null);
						}
					}
					reportAuthRes.save(reportAuthList);
				}
    			return request(super.createRequestPageTempletResponse("/apps/business/report/pbreport/auth/disorganlist"));
    		}else{
    			ReportAuth dicAuth = reportAuthRes.findByDicidAndOrgi(reportauth.getDatadicid(), super.getOrgi(request));
    			if (dicAuth != null && !StringUtils.isBlank(dicAuth.getAuthorgan())) {
					String[] oids = dicAuth.getAuthorgan().split(",");
					if (oids != null && oids.length > 0) {
						map.put("organList", organRes.findByIdInAndOrgi(Arrays.asList(oids), super.getOrgi(request)));
					}
				}
    			return request(super.createRequestPageTempletResponse("/apps/business/report/pbreport/auth/rorganform"));
    		}
		}
    	if (!StringUtils.isBlank(authtype) && "role".equals(authtype)) {
    		if (reportauth != null && !StringUtils.isBlank(reportauth.getAuthrole())) {
    			String[] ids = reportauth.getAuthrole().split(",");
    			List<String> idList = new ArrayList<>();
    			if (ids != null && ids.length>0 && !StringUtils.isBlank(delid)) {
    				for(String idstr:ids){
    					if (!delid.equals(idstr)) {
    						idList.add(idstr);
						}
    				}
    				reportauth.setAuthrole(idList.size()>0?idList.toString().replace("[", "").replace("]", "").replaceAll(" ", ""):null);
    				reportAuthRes.save(reportauth);
    				if (idList.size() >0) {
    					map.put("disroleList", roleRes.findByIdInAndOrgi(idList, super.getOrgi(request)));
					}
				}
			}
    		map.put("reportauth", reportauth);
    		if (!StringUtils.isBlank(type) && "dataDic".equals(type)) {
    			List<ReportAuth> reportAuthList = reportAuthRes.findByDatadicidAndTypeAndOrgi(reportauth.getDicid(), "report", super.getOrgi(request));
    			if (reportAuthList != null && reportAuthList.size() > 0) {
					for(ReportAuth rauth : reportAuthList){
						String[] ids = rauth.getAuthrole().split(",");
		    			List<String> idList = new ArrayList<>();
		    			if (ids != null && ids.length>0 && !StringUtils.isBlank(delid)) {
		    				for(String idstr:ids){
		    					if (!delid.equals(idstr)) {
		    						idList.add(idstr);
								}
		    				}
		    				rauth.setAuthrole(idList.size()>0?idList.toString().replace("[", "").replace("]", "").replaceAll(" ", ""):null);
						}
					}
					reportAuthRes.save(reportAuthList);
				}
    			map.put("roleList", roleRes.findByOrgi(super.getOrgi(request)));
    		}else{
    			ReportAuth dicAuth = reportAuthRes.findByDicidAndOrgi(reportauth.getDatadicid(), super.getOrgi(request));
    			if (dicAuth != null && !StringUtils.isBlank(dicAuth.getAuthrole())) {
					String[] oids = dicAuth.getAuthrole().split(",");
					if (oids != null && oids.length > 0) {
						map.put("roleList", roleRes.findByIdInAndOrgi(Arrays.asList(oids), super.getOrgi(request)));
					}
				}
    		}
    		return request(super.createRequestPageTempletResponse("/apps/business/report/pbreport/auth/roleform"));
		}
    	if (!StringUtils.isBlank(authtype) && "user".equals(authtype)) {
    		
    		if (reportauth != null && !StringUtils.isBlank(reportauth.getAuthuser())) {
    			String[] ids = reportauth.getAuthuser().split(",");
    			List<String> idList = new ArrayList<>();
    			if (ids != null && ids.length>0 && !StringUtils.isBlank(delid)) {
    				for(String idstr:ids){
    					if (!delid.equals(idstr)) {
    						idList.add(idstr);
						}
    				}
    				reportauth.setAuthuser(idList.size()>0?idList.toString().replace("[", "").replace("]", "").replaceAll(" ", ""):null);
    				reportAuthRes.save(reportauth);
    				if (idList.size() >0) {
    					map.put("disuserList", userRes.findByIdInAndOrgi(idList, super.getOrgi(request)));
					}
				}
			}
    		map.put("reportauth", reportauth);
    		if (!StringUtils.isBlank(type) && "dataDic".equals(type)) {
    			List<ReportAuth> reportAuthList = reportAuthRes.findByDatadicidAndTypeAndOrgi(reportauth.getDicid(), "report", super.getOrgi(request));
    			if (reportAuthList != null && reportAuthList.size() > 0) {
					for(ReportAuth rauth : reportAuthList){
						String[] ids = rauth.getAuthuser().split(",");
		    			List<String> idList = new ArrayList<>();
		    			if (ids != null && ids.length>0 && !StringUtils.isBlank(delid)) {
		    				for(String idstr:ids){
		    					if (!delid.equals(idstr)) {
		    						idList.add(idstr);
								}
		    				}
		    				rauth.setAuthuser(idList.size()>0?idList.toString().replace("[", "").replace("]", "").replaceAll(" ", ""):null);
						}
					}
					reportAuthRes.save(reportAuthList);
				}
    			map.put("userList", userRes.findByOrgiAndDatastatus(super.getOrgi(request), false));
    		}else{
    			ReportAuth dicAuth = reportAuthRes.findByDicidAndOrgi(reportauth.getDatadicid(), super.getOrgi(request));
    			if (dicAuth != null && !StringUtils.isBlank(dicAuth.getAuthuser())) {
					String[] oids = dicAuth.getAuthuser().split(",");
					if (oids != null && oids.length > 0) {
						map.put("userList", userRes.findByIdInAndOrgi(Arrays.asList(oids), super.getOrgi(request)));
					}
				}
    		}
    		return request(super.createRequestPageTempletResponse("/apps/business/report/pbreport/auth/userform"));
		}
		return request(super.createRequestPageTempletResponse("/apps/business/report/pbreport/auth/organ"));
	}
	
	@RequestMapping("/organ")
    @Menu(type = "apps" , subtype = "report")
    public ModelAndView callagentorganlist(ModelMap map , HttpServletRequest request , @Valid String organ, @Valid String id, @Valid String type) {
    	
    	map.addAttribute("currentorgan",organ);
    	
    	List<Organ> organList = null;
    	ReportAuth reportauth =  null;
    	if (!StringUtils.isBlank(type) && "dataDic".equals(type)) {
    		reportauth = reportAuthRes.findByDicidAndOrgi(id, super.getOrgi(request));
		}else{
			reportauth = reportAuthRes.findByReportAndOrgi(id, super.getOrgi(request));
		}
    	if (reportauth != null && !StringUtils.isBlank(reportauth.getAuthorgan())) {
    		String[] organids = reportauth.getAuthorgan().split(",");
    		if (organids != null && organids.length > 0) {
    			organList = organRes.findByOrgiAndParentAndIdNotIn(super.getOrgi(request),organ,Arrays.asList(organids));
			}
		}else{
			organList = organRes.findByOrgiAndParent(super.getOrgi(request), organ);
		}
    	map.addAttribute("organList", organList);
    	
    	return request(super.createRequestPageTempletResponse("/apps/business/report/pbreport/auth/organlist")) ; 
    }
	
	@RequestMapping("/auth/editreport")
	@Menu(type = "apps" , subtype = "report")
	public ModelAndView editreport(ModelMap map , HttpServletRequest request , @Valid String dicid, @Valid String reportid) {
    	map.put("dicid", dicid);
    	map.put("report", publishedReportRes.findByIdAndOrgi(reportid, super.getOrgi(request)));
		return request(super.createRequestPageTempletResponse("/apps/business/report/pbreport/auth/editreport"));
	}
	
	@RequestMapping("/auth/editreport/save")
	@Menu(type = "apps" , subtype = "report")
	public ModelAndView editreportsave(ModelMap map , HttpServletRequest request , @Valid PublishedReport publishedReport, @Valid String dicid) {
		PublishedReport publishedreport = publishedReportRes.findByIdAndOrgi(publishedReport.getId(), super.getOrgi(request));
		if (publishedreport != null ) {
			publishedreport.setUserauth(publishedReport.isUserauth());
			publishedReportRes.save(publishedreport);
		}
		return request(super.createRequestPageTempletResponse("redirect:/apps/report/pbreportindex.html?dicid="+dicid));
	}
}