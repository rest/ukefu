package com.ukefu.webim.web.handler.api.rest;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ukefu.core.UKDataContext;
import com.ukefu.util.Menu;
import com.ukefu.webim.service.acd.ServiceQuene;
import com.ukefu.webim.service.cache.CacheHelper;
import com.ukefu.webim.service.es.UserEventRepository;
import com.ukefu.webim.service.repository.AgentUserRepository;
import com.ukefu.webim.util.RestResult;
import com.ukefu.webim.util.RestResultType;
import com.ukefu.webim.web.handler.Handler;
import com.ukefu.webim.web.handler.api.request.SearchData;
import com.ukefu.webim.web.model.AgentUser;
import com.ukefu.webim.web.model.UserHistory;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/agentuser")
@Api(value = "ACD服务", description = "获取当前对话中的访客")
public class ApiAgentUserController extends Handler{
	
	@Autowired
	private AgentUserRepository agentUserRepository ;
	
	@Autowired
	private UserEventRepository userEventRes; 
	
	/**
	 * 获取当前对话中的访客
	 * @param request
	 * @return
	 */
	@RequestMapping( method = RequestMethod.GET)
	@Menu(type = "apps" , subtype = "agentuser" , access = true)
	@ApiOperation("获取当前正在对话的访客信息，包含多种渠道来源的访客")
    public ResponseEntity<RestResult> list(HttpServletRequest request , @Valid String q) {
        return new ResponseEntity<>(new RestResult(RestResultType.OK , agentUserRepository.findByAgentnoAndOrgi(super.getUser(request).getId() , super.getOrgi(request) , new Sort(Direction.ASC,"status"))), HttpStatus.OK);
    }
	
	@RequestMapping(value = "/inservice")
	@Menu(type = "apps" , subtype = "agentuser" , access = true)
	@ApiOperation("获取当前正在对话的访客信息，包含多种渠道来源的访客,访客还在线")
    public ResponseEntity<RestResult> inserviceList(HttpServletRequest request , @Valid String q) {
		Page<AgentUser> dataList = agentUserRepository.findByOrgiAndStatus(super.getOrgi(request), UKDataContext.AgentUserStatusEnum.INSERVICE.toString() ,new PageRequest(super.getP(request), super.getPs(request), Direction.DESC , "createtime")) ;
		return new ResponseEntity<>(new RestResult(RestResultType.OK , new SearchData<AgentUser>(dataList)), HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/service/quene/index")
	@Menu(type = "apps" , subtype = "agentuser" , access = true)
	@ApiOperation("排队中的访客列表")
	public ResponseEntity<RestResult> queneIndex(HttpServletRequest request) {
		Page<AgentUser> dataList = agentUserRepository.findByOrgiAndStatus(super.getOrgi(request), UKDataContext.AgentUserStatusEnum.INQUENE.toString() ,new PageRequest(super.getP(request), super.getPs(request), Direction.DESC , "createtime")) ;
		return new ResponseEntity<>(new RestResult(RestResultType.OK , new SearchData<AgentUser>(dataList)), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/service/quene/clean")
	@Menu(type = "apps" , subtype = "agentuser" , access = true)
	@ApiOperation("排队中的访客,转队列")
	public ResponseEntity<RestResult> quene(HttpServletRequest request , @Valid String id) {
		AgentUser agentUser = agentUserRepository.findByIdAndOrgi(id, super.getOrgi(request)) ;
		if(agentUser!=null && agentUser.getStatus().equals(UKDataContext.AgentUserStatusEnum.INQUENE.toString())){
			agentUser.setAgent(null);
			agentUser.setSkill(null);
			agentUserRepository.save(agentUser) ;
			CacheHelper.getAgentUserCacheBean().put(agentUser.getUserid(), agentUser, super.getOrgi(request));
			ServiceQuene.allotAgent(agentUser, super.getOrgi(request)) ;
		}
		return new ResponseEntity<>(new RestResult(RestResultType.OK), HttpStatus.OK);
	}
	@RequestMapping(value = "/service/quene/invite")
	@Menu(type = "apps" , subtype = "agentuser" , access = true)
	@ApiOperation("排队中的访客,邀请对话")
	public ResponseEntity<RestResult> invite(HttpServletRequest request , @Valid String id) throws Exception {
		AgentUser agentUser = agentUserRepository.findByIdAndOrgi(id, super.getOrgi(request)) ;
		if(agentUser!=null && agentUser.getStatus().equals(UKDataContext.AgentUserStatusEnum.INQUENE.toString())){
			ServiceQuene.allotAgentForInvite(super.getUser(request).getId() , agentUser, super.getOrgi(request)) ;
		}
		return new ResponseEntity<>(new RestResult(RestResultType.OK), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/service/trace")
	@Menu(type = "apps" , subtype = "agentuser" , access = true)
	@ApiOperation("排队中的访客,轨迹")
	public ResponseEntity<RestResult> trace(HttpServletRequest request , @Valid String sessionid) {
		Page<UserHistory> dataList = null;
		if(!StringUtils.isBlank(sessionid)){
			dataList = userEventRes.findBySessionidAndOrgi(sessionid, super.getOrgi(request), new PageRequest(super.getP(request), super.getPs(request)));
		}
		return new ResponseEntity<>(new RestResult(RestResultType.OK , new SearchData<UserHistory>(dataList)), HttpStatus.OK);
	}
	

}