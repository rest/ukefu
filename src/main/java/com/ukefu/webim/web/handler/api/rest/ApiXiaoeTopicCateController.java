package com.ukefu.webim.web.handler.api.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ukefu.util.Menu;
import com.ukefu.webim.service.es.TopicRepository;
import com.ukefu.webim.service.repository.KnowledgeTypeRepository;
import com.ukefu.webim.util.RestResult;
import com.ukefu.webim.util.RestResultType;
import com.ukefu.webim.web.handler.Handler;
import com.ukefu.webim.web.model.KnowledgeType;
import com.ukefu.webim.web.model.Topic;

@RestController
@RequestMapping("/api/xiaoe/cate")
@Api(value = "智能机器人", description = "知识库分类维护")
public class ApiXiaoeTopicCateController extends Handler{

	@Autowired
	private KnowledgeTypeRepository knowledgeTypeRes ;
	

	@Autowired
	private TopicRepository topicRepository;
	
	/**
	 * 知识库管理功能
	 * @param request
	 * @param username	搜索用户名，精确搜索
	 * @return
	 */
	@RequestMapping( method = RequestMethod.GET)
	@Menu(type = "apps" , subtype = "topic" , access = true)
	@ApiOperation("返回分类列表")
    public ResponseEntity<RestResult> list(HttpServletRequest request , @Valid String cate, @Valid String typeid, @Valid String q) {
        return new ResponseEntity<>(new RestResult(RestResultType.OK, knowledgeTypeRes.findByOrgiAndTypeidOrderBySortindexAsc(super.getOrgi(request) , typeid)), HttpStatus.OK);
    }
	
	/**
	 * 新增或修改分类
	 * @param request
	 * @param user
	 * @return
	 */
	@RequestMapping(method = RequestMethod.PUT)
	@Menu(type = "apps" , subtype = "topic" , access = true)
	@ApiOperation("新增/编辑知识分类")
    public ResponseEntity<RestResult> put(HttpServletRequest request , @Valid KnowledgeType type) {
    	if(type != null && !StringUtils.isBlank(type.getName())){
    		type.setOrgi(super.getOrgi(request));
    		knowledgeTypeRes.save(type) ;
    	}
        return new ResponseEntity<>(new RestResult(RestResultType.OK), HttpStatus.OK);
    }
	
	/**
	 * 删除分类
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(method = RequestMethod.DELETE)
	@Menu(type = "apps" , subtype = "topic" , access = true)
	@ApiOperation("删除知识库分类")
    public ResponseEntity<RestResult> delete(HttpServletRequest request , @Valid String id) {
		RestResult result = new RestResult(RestResultType.OK) ; 
    	KnowledgeType type = null ;
    	if(!StringUtils.isBlank(id)){
    		type = knowledgeTypeRes.findByIdAndOrgi(id,super.getOrgi(request)) ;
    		if(type!=null){
    			Page<Topic> page = topicRepository.getTopicByCateAndOrgi(id, super.getOrgi(request),null, super.getP(request), super.getPs(request)) ;
    	    	if(page.getTotalElements() == 0){
    	    		knowledgeTypeRes.delete(id);
    	    	}else{
    	    		result.setStatus(RestResultType.XIAOE_TOPIC_NOT_EMPTY);
    	    	}
    		}else{
    			result.setStatus(RestResultType.XIAOE_TYPE_DELETE);
    		}
    	}
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}