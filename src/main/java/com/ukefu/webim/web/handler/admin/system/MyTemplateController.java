package com.ukefu.webim.web.handler.admin.system;

import com.ukefu.core.UKDataContext;
import com.ukefu.util.Menu;
import com.ukefu.util.UKTools;
import com.ukefu.webim.service.cache.CacheHelper;
import com.ukefu.webim.service.repository.SysDicRepository;
import com.ukefu.webim.service.repository.TemplateRepository;
import com.ukefu.webim.web.handler.Handler;
import com.ukefu.webim.web.model.SysDic;
import com.ukefu.webim.web.model.Template;
import com.ukefu.webim.web.model.UKeFuDic;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/admin/mytemplate")
public class MyTemplateController extends Handler{
	
	
	@Autowired
	private TemplateRepository templateRes;
	
	@Autowired
	private SysDicRepository dicRes;

    @RequestMapping("/index")
    @Menu(type = "admin" , subtype = "template" , access = false , admin = true)
    public ModelAndView index(ModelMap map , HttpServletRequest request) {
		List<SysDic> list = UKeFuDic.getInstance().getDic(UKDataContext.UKEFU_SYSTEM_DIC);

		List<SysDic> resultList = new ArrayList<>();
		if(list.size() > 0){
			for(SysDic sysdic : list){
				if(sysdic.isTenant()){
					resultList.add(sysdic);
				}
			}
		}
    	map.addAttribute("sysDicList",resultList );
        return request(super.createAdminTempletResponse("/admin/system/template/myindex"));
    }
    

    @RequestMapping("/list")
    @Menu(type = "admin" , subtype = "template" , access = false , admin = true)
    public ModelAndView list(ModelMap map , HttpServletRequest request ,@Valid String type) {
    	map.addAttribute("sysDic", dicRes.findById(type));
    	map.addAttribute("templateList", templateRes.findByTemplettypeAndOrgi(type, super.getOrgi(request)));
        return request(super.createAdminTempletResponse("/admin/system/template/mylist"));
    }
    
    @RequestMapping("/add")
    @Menu(type = "admin" , subtype = "template" , access = false , admin = true )
    public ModelAndView add(ModelMap map , HttpServletRequest request ,@Valid String type) {
    	map.addAttribute("sysDic", dicRes.findById(type));
        return request(super.createRequestPageTempletResponse("/admin/system/template/myadd"));
    }
    
    @RequestMapping(  "/save")
    @Menu(type = "admin" , subtype = "template" , access = false , admin = true )
    public ModelAndView save(HttpServletRequest request  , @Valid Template template) {
    	template.setOrgi(super.getOrgi(request));
    	template.setCreatetime(new Date());
    	
    	SysDic dic = dicRes.findById(template.getTemplettype());
		if(dic!=null && StringUtils.isBlank(template.getCode())) {
			template.setCode(dic.getCode());
		}
    	templateRes.save(template) ;
    	
		return request(super.createRequestPageTempletResponse("redirect:/admin/mytemplate/list.html?type="+template.getTemplettype()));
    }
    
    @RequestMapping("/edit")
    @Menu(type = "admin" , subtype = "template" , access = false , admin = true )
    public ModelAndView edit(ModelMap map , HttpServletRequest request , @Valid String id, @Valid String type) {
    	map.addAttribute("sysDic", dicRes.findById(type));
    	map.addAttribute("template", templateRes.findByIdAndOrgi(id, super.getOrgi(request))) ;
        return request(super.createRequestPageTempletResponse("/admin/system/template/myedit"));
    }
    
    @RequestMapping(  "/update")
    @Menu(type = "admin" , subtype = "template" , access = false , admin = true )
    public ModelAndView update(HttpServletRequest request  , @Valid Template template) {
    	Template oldTemplate = templateRes.findByIdAndOrgi(template.getId(), super.getOrgi(request)) ;
    	if(oldTemplate!=null){
    		SysDic dic = dicRes.findById(oldTemplate.getTemplettype());
    		if(dic!=null) {
    			oldTemplate.setCode(dic.getCode());
    		}
    		if(!StringUtils.isBlank(template.getCode())) {
    			oldTemplate.setCode(template.getCode());
    		}
    		oldTemplate.setName(template.getName());
    		oldTemplate.setLayoutcols(template.getLayoutcols());
    		oldTemplate.setIconstr(template.getIconstr());
    		oldTemplate.setDatatype(template.getDatatype());
    		oldTemplate.setCharttype(template.getCharttype());
    		templateRes.save(oldTemplate) ;
    		
    		CacheHelper.getSystemCacheBean().delete(template.getId(), super.getOrgi(request)) ;
    	}
		return request(super.createRequestPageTempletResponse("redirect:/admin/mytemplate/list.html?type="+template.getTemplettype()));
    }
    
    @RequestMapping("/code")
    @Menu(type = "admin" , subtype = "template" , access = false , admin = true )
    public ModelAndView code(ModelMap map , HttpServletRequest request , @Valid String id, @Valid String type) {
    	map.addAttribute("sysDic", dicRes.findById(type));
    	map.addAttribute("template", templateRes.findByIdAndOrgi(id, super.getOrgi(request))) ;
        return request(super.createRequestPageTempletResponse("/admin/system/template/mycode"));
    }
    
    @RequestMapping(  "/codesave")
    @Menu(type = "admin" , subtype = "template" , access = false , admin = true )
    public ModelAndView codesave(HttpServletRequest request  , @Valid Template template) {
    	Template oldTemplate = templateRes.findByIdAndOrgi(template.getId(), super.getOrgi(request)) ;
    	if(oldTemplate!=null){
    		oldTemplate.setTemplettext(template.getTemplettext());
    		oldTemplate.setTemplettitle(template.getTemplettitle());
    		templateRes.save(oldTemplate) ;
    		
    		CacheHelper.getSystemCacheBean().delete(template.getId(), super.getOrgi(request)) ;
    	}
		return request(super.createRequestPageTempletResponse("redirect:/admin/mytemplate/list.html?type="+template.getTemplettype()));
    }
    
    @RequestMapping("/delete")
    @Menu(type = "admin" , subtype = "template" , access = false , admin = true)
    public ModelAndView delete(HttpServletRequest request ,@Valid Template template) {
    	if(template!=null){
    		templateRes.delete(template) ;
    		
    		CacheHelper.getSystemCacheBean().delete(template.getId(), super.getOrgi(request)) ;
    	}
    	return request(super.createRequestPageTempletResponse("redirect:/admin/mytemplate/list.html?type="+template.getTemplettype()));
    }
    
}