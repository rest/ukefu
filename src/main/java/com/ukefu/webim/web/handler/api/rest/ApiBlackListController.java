package com.ukefu.webim.web.handler.api.rest;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ukefu.core.UKDataContext;
import com.ukefu.util.Menu;
import com.ukefu.util.UKTools;
import com.ukefu.webim.service.cache.CacheHelper;
import com.ukefu.webim.service.repository.BlackListRepository;
import com.ukefu.webim.service.repository.UserRepository;
import com.ukefu.webim.util.RestResult;
import com.ukefu.webim.util.RestResultType;
import com.ukefu.webim.web.handler.Handler;
import com.ukefu.webim.web.handler.api.request.SearchData;
import com.ukefu.webim.web.model.AgentUser;
import com.ukefu.webim.web.model.BlackEntity;
import com.ukefu.webim.web.model.Contacts;
import com.ukefu.webim.web.model.User;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/blacklist")
@Api(value = "用户服务", description = "黑名单管理功能")
public class ApiBlackListController extends Handler{

	@Autowired
	private BlackListRepository blackListRes;

	/**
	 * 返回用户列表，支持分页，分页参数为 p=1&ps=50，默认分页尺寸为 20条每页
	 * @param request
	 * @param username	搜索用户名，精确搜索
	 * @return
	 */
	@RequestMapping( method = RequestMethod.GET)
	@Menu(type = "apps" , subtype = "user" , access = true)
	@ApiOperation("返回用户列表，支持分页，分页参数为 p=1&ps=50，默认分页尺寸为 20条每页")
    public ResponseEntity<RestResult> list(HttpServletRequest request , @Valid String id ,@Valid String username) {
		Page<BlackEntity> dataList = blackListRes.findByOrgi(super.getOrgi(request), new PageRequest(super.getP(request), super.getPs(request), Sort.Direction.DESC, "endtime"));
        return new ResponseEntity<>(new RestResult(RestResultType.OK, new SearchData<BlackEntity>(dataList)), HttpStatus.OK);
    }
	
	@RequestMapping(value = "/delete")
	@Menu(type = "apps" , subtype = "agentuser" , access = true)
	@ApiOperation("排队中的访客列表")
	public ResponseEntity<RestResult> queneIndex(HttpServletRequest request ,String id) {
		if(!StringUtils.isBlank(id)){
    		BlackEntity tempBlackEntity = blackListRes.findByIdAndOrgi(id, super.getOrgi(request)) ;
    		if(tempBlackEntity!=null){
		    	blackListRes.delete(tempBlackEntity);
		    	if (!StringUtils.isBlank(tempBlackEntity.getUserid())) {
		    		CacheHelper.getSystemCacheBean().delete(tempBlackEntity.getUserid(), UKDataContext.SYSTEM_ORGI) ;
				}
    		}
    	}
		return new ResponseEntity<>(new RestResult(RestResultType.OK ), HttpStatus.OK);
	}
}