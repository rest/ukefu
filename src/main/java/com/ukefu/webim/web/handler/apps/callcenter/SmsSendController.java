package com.ukefu.webim.web.handler.apps.callcenter;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ukefu.core.UKDataContext;
import com.ukefu.util.Menu;
import com.ukefu.util.UKTools;
import com.ukefu.util.sms.CommandSms;
import com.ukefu.webim.service.cache.CacheHelper;
import com.ukefu.webim.service.repository.UserRepository;
import com.ukefu.webim.web.handler.Handler;
import com.ukefu.webim.web.model.SmsTemplate;
import com.ukefu.webim.web.model.StatusEvent;
import com.ukefu.webim.web.model.SystemConfig;
import com.ukefu.webim.web.model.User;

@Controller
@RequestMapping("/apps/sms")
public class SmsSendController extends Handler{
	
	@Autowired
	private UserRepository userRes ;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@RequestMapping(value = "/send")
    @Menu(type = "callcenter" , subtype = "sms" , access = true)
    public ModelAndView index(ModelMap map , HttpServletRequest request , @Valid String id , @Valid String tpid) {
		ModelAndView view = request(super.createRequestPageTempletResponse("/apps/business/callcenter/sms")) ;
		StatusEvent statusEvent = (StatusEvent) CacheHelper.getCallCenterCacheBean().getCacheObject(id, UKDataContext.SYSTEM_ORGI) ;
		if(statusEvent!=null) {
			SmsTemplate smsTemplate = UKTools.getSmsTemplate(tpid , statusEvent.getOrgi()) ;
			if(smsTemplate!=null && !StringUtils.isBlank(statusEvent.getPriphone())) {
				map.addAttribute("statusEvent" , statusEvent);
				UKTools.published(new CommandSms(statusEvent.getPriphone() , tpid , null , smsTemplate.getTemplettype(), smsTemplate.getOrgi(),super.getUser(request).getId()));
			}
		}
		return view ;
	}
	
	@RequestMapping(value = "/tip/send")
    @Menu(type = "callcenter" , subtype = "tipsms" , access = false)
    public ModelAndView send(ModelMap map , HttpServletRequest request,@Valid String data) {
		ModelAndView view = request(super.createRequestPageTempletResponse("/apps/business/callcenter/sms")) ;
		SystemConfig systemConfig = UKTools.getSystemConfig() ;
		if(!StringUtils.isBlank(systemConfig.getReqlogwarningtouser())) {
			logger.info("Error Message : "+ data);
			String[] users = systemConfig.getReqlogwarningtouser().split(",") ;
			List<User> userList = userRes.findAll(Arrays.asList(users)) ;
			if(!StringUtils.isBlank(systemConfig.getReqlogwarningsmsid()) && !StringUtils.isBlank(systemConfig.getReqlogwarningsmstp())) {
				SmsTemplate smsTemplate = UKTools.getSmsTemplate(systemConfig.getReqlogwarningsmstp() , systemConfig.getOrgi()) ;
				for(User user : userList) {
					if(smsTemplate!=null && !StringUtils.isBlank(user.getMobile())) {
						UKTools.published(new CommandSms(user.getMobile() , systemConfig.getReqlogwarningsmstp() , null , smsTemplate.getTemplettype(), smsTemplate.getOrgi(),super.getUser(request).getId()));
					}
				}
			}
		}
		return view ;
	}
}
