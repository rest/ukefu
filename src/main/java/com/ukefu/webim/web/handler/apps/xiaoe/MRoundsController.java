package com.ukefu.webim.web.handler.apps.xiaoe;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ukefu.util.Menu;
import com.ukefu.webim.service.es.MRoundAnswerRepository;
import com.ukefu.webim.service.es.MRoundQuestionRepository;
import com.ukefu.webim.service.es.TopicRepository;
import com.ukefu.webim.service.repository.SceneRepository;
import com.ukefu.webim.service.repository.ServiceAiRepository;
import com.ukefu.webim.web.handler.Handler;
import com.ukefu.webim.web.model.MRoundQuestion;

@Controller
@RequestMapping("/apps/xiaoe/mrounds")
public class MRoundsController extends Handler{
	
	@Value("${web.upload-path}")
    private String path;
	
	@Value("${uk.im.server.port}")  
    private Integer port; 
	@Autowired
	private ServiceAiRepository serviceAiRes ;
	
	@Autowired
	private TopicRepository topicRes;
	
	@Autowired
	private SceneRepository sceneRes ;
	
	@Autowired
	private MRoundQuestionRepository mroundRes ;
	
	@Autowired
	private MRoundAnswerRepository mroundAndwerRes ;

	@RequestMapping("/index")
    @Menu(type = "xiaoe" , subtype = "mrounds")
    public ModelAndView list(ModelMap map , HttpServletRequest request , @Valid String id , String type) throws IOException {
    	map.put("serviceAiList", serviceAiRes.findByOrgi(super.getOrgi(request), new PageRequest(super.getP(request), super.getPs(request), Sort.Direction.DESC, new String[] { "createtime" }))) ;
    	processData(map, request, id, type);
    	
    	return request(super.createAppsTempletResponse("/apps/business/xiaoe/mrounds/index"));
    }
	
	private void processData(ModelMap map , HttpServletRequest request , @Valid String dataid , String type) {
		if(!StringUtils.isBlank(dataid) && !StringUtils.isBlank(type)) {
	    	if(type.equals("topic")) {
	    		map.put("topic", topicRes.findByIdAndOrgi(dataid,super.getOrgi(request))) ;
	    	}else if(type.equals("scene")) {
	    		map.put("scene", sceneRes.findByIdAndOrgi(dataid,super.getOrgi(request))) ;
	    	}
	    	map.put("type", type) ;
	    	map.put("dataid", dataid) ;
	    	map.put("mroundQuestionList", mroundRes.findByDataidAndOrgi(dataid, super.getOrgi(request))) ;
	    	
	    	map.put("mroundAnswerList", mroundAndwerRes.findByDataidAndOrgi(dataid, super.getOrgi(request))) ;
    	}
	}
	
	@RequestMapping("/add")
    @Menu(type = "xiaoe" , subtype = "mrounds")
    public ModelAndView add(ModelMap map , HttpServletRequest request , @Valid String dataid , String type, String parentid) {
		if(!StringUtils.isBlank(dataid) && !StringUtils.isBlank(type)) {
	    	if(type.equals("topic")) {
	    		map.put("topic", topicRes.findByIdAndOrgi(dataid,super.getOrgi(request))) ;
	    	}else if(type.equals("scene")) {
	    		map.put("scene", sceneRes.findByIdAndOrgi(dataid,super.getOrgi(request))) ;
	    	}
	    	map.put("type", type) ;
	    	map.put("dataid", dataid) ;
	    	map.put("parentid", parentid) ;
    	}
    	return request(super.createRequestPageTempletResponse("/apps/business/xiaoe/mrounds/add"));
    }
	
	@RequestMapping("/save")
    @Menu(type = "xiaoe" , subtype = "mrounds")
    public ModelAndView save(ModelMap map , HttpServletRequest request ,@Valid MRoundQuestion mround) {
    	if(mround!=null && !StringUtils.isBlank(mround.getTitle())){
    		mround.setOrgi(super.getOrgi(request));
    		mround.setCreater(super.getUser(request).getId());
    		mround.setCreatetime(new Date());
    		mroundRes.save(mround) ;
    	}
    	return request(super.createRequestPageTempletResponse("redirect:/apps/xiaoe/mrounds/index.html?id="+mround.getDataid()+"&dataid="+mround.getDataid()+"&type="+mround.getMtype()));
    }
	
	@RequestMapping("/edit")
    @Menu(type = "xiaoe" , subtype = "mrounds")
    public ModelAndView edit(ModelMap map , HttpServletRequest request , @Valid String dataid , String type, String id, String debug) {
		if(!StringUtils.isBlank(dataid) && !StringUtils.isBlank(type)) {
	    	if(type.equals("topic")) {
	    		map.put("topic", topicRes.findByIdAndOrgi(dataid,super.getOrgi(request))) ;
	    	}else if(type.equals("scene")) {
	    		map.put("scene", sceneRes.findByIdAndOrgi(dataid,super.getOrgi(request))) ;
	    	}
	    	map.put("type", type) ;
	    	map.put("dataid", dataid) ;
	    	
	    	if(!StringUtils.isBlank(debug)) {
	    		map.put("debug", true) ;
	    	}
	    	
	    	map.put("mround", mroundRes.findById(id)) ;
    	}
		
    	return request(super.createRequestPageTempletResponse("/apps/business/xiaoe/mrounds/edit"));
    }
	
	@RequestMapping("/update")
    @Menu(type = "xiaoe" , subtype = "mrounds")
    public ModelAndView update(ModelMap map , HttpServletRequest request , @Valid String dataid , String type,@Valid MRoundQuestion mround, String debug) {
		ModelAndView view = request(super.createRequestPageTempletResponse("redirect:/apps/xiaoe/mrounds/index.html?id="+mround.getDataid()+"&dataid="+mround.getDataid()+"&type="+mround.getMtype())) ;
    	if(!StringUtils.isBlank(debug)) {
    		view = request(super.createRequestPageTempletResponse("/public/success")) ;
    	}
		if(mround!=null && !StringUtils.isBlank(mround.getTitle())){
			MRoundQuestion temp = mroundRes.findById(mround.getId()) ;
			if(temp!=null) {
				if(StringUtils.isBlank(temp.getParentid())) {
					mround.setParentid(mround.getDataid());	
				}else {
					mround.setParentid(temp.getParentid());
				}
				mround.setOrgi(super.getOrgi(request));
	    		mround.setCreater(temp.getCreater());
	    		mround.setCreatetime(temp.getCreatetime());
	    		mround.setUpdatetime(new Date());
			}else {
				if(!StringUtils.isBlank(mround.getParentid())) {
					mround.setParentid(dataid);
				}
				mround.setId(dataid);
				mround.setOrgi(super.getOrgi(request));
	    		mround.setCreater(super.getUser(request).getId());
	    		mround.setCreatetime(new Date());
	    		mround.setUpdatetime(new Date());
			}
			mroundRes.save(mround) ;
    	}
		return view;
    }
	
	
	@RequestMapping("/drag")
    @Menu(type = "xiaoe" , subtype = "mrounds")
    public ModelAndView drag(ModelMap map , HttpServletRequest request , @Valid String dataid , String type,@Valid String source ,@Valid String to) {
		if(!StringUtils.isBlank(source) && !StringUtils.isBlank(to)){
			MRoundQuestion temp = mroundRes.findById(source) ;
			if(temp!=null) {
				temp.setParentid(to);
				mroundRes.save(temp) ;
			}
    	}
		return request(super.createRequestPageTempletResponse("redirect:/apps/xiaoe/mrounds/index.html?id="+dataid+"&dataid="+dataid+"&type="+type));
    }
	
	@RequestMapping("/delete")
    @Menu(type = "xiaoe" , subtype = "mrounds")
    public ModelAndView delete(ModelMap map , HttpServletRequest request , @Valid String dataid , String type, String id, String debug) {
		if(!StringUtils.isBlank(dataid) && !StringUtils.isBlank(type)) {
			List<MRoundQuestion> mRoundsList = mroundRes.findByDataidAndOrgi(dataid, super.getOrgi(request)) ;
			mroundRes.delete(id) ;
			List<MRoundQuestion> deleteMroundsList = new ArrayList<MRoundQuestion>();
			
			matchChild(mRoundsList, deleteMroundsList, id);
			if(deleteMroundsList.size() > 0) {
				mroundRes.delete(deleteMroundsList);
			}
    	}
		return request(super.createRequestPageTempletResponse("redirect:/apps/xiaoe/mrounds/index.html?id="+dataid+"&dataid="+dataid+"&type="+type));
    }
	/**
	 * 
	 * @param mRoundsList
	 * @param deleteMroundsList
	 * @param id
	 */
	public void matchChild(List<MRoundQuestion> mRoundsList , List<MRoundQuestion> deleteMroundsList, String id) {
		for(MRoundQuestion mRoundsQuestion : mRoundsList) {
			if(mRoundsQuestion.getParentid().equals(id)) {
				deleteMroundsList.add(mRoundsQuestion) ;
				matchChild(mRoundsList, deleteMroundsList, mRoundsQuestion.getId());
			}
		}
	}
}