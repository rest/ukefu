package com.ukefu.webim.web.handler.api.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ukefu.core.UKDataContext;
import com.ukefu.util.Menu;
import com.ukefu.webim.util.RestResult;
import com.ukefu.webim.util.RestResultType;
import com.ukefu.webim.util.impl.AiMessageProcesserImpl;
import com.ukefu.webim.util.server.message.ChatMessage;
import com.ukefu.webim.web.handler.Handler;

@RestController
@RequestMapping("/ai/xiaoe")
@Api(value = "智能机器人", description = "获取对话内容")
public class ApiXiaoeController extends Handler{

	@Autowired
	private AiMessageProcesserImpl processer;
	
	/**
	 * 知识库管理功能
	 * @param request
	 * @param username	搜索用户名，精确搜索
	 * @return
	 */
	@RequestMapping
	@Menu(type = "apps" , subtype = "xiaoe" , access = true)
	@ApiOperation("获取对话内容")
    public ResponseEntity<RestResult> chat(HttpServletRequest request , @Valid String message , @Valid String userid , @Valid String orgi) {
		ChatMessage chatMessage = null ;
		if(!StringUtils.isBlank(userid) && !StringUtils.isBlank(orgi)){
			chatMessage = processer.chat(message, userid, null , UKDataContext.ChannelTypeEnum.WEBIM.toString(), UKDataContext.MediaTypeEnum.TEXT.toString(), request.getSession().getId(), orgi , request.getSession().getId()) ;
		}
        return new ResponseEntity<>(new RestResult(RestResultType.OK,chatMessage), HttpStatus.OK);
    }
}