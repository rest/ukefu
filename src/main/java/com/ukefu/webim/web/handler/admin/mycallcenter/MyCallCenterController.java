package com.ukefu.webim.web.handler.admin.mycallcenter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ukefu.core.UKDataContext;
import com.ukefu.util.Menu;
import com.ukefu.util.extra.CallCenterInterface;
import com.ukefu.webim.service.repository.PbxHostOrgiRelaRepository;
import com.ukefu.webim.service.repository.PbxHostRepository;
import com.ukefu.webim.web.handler.Handler;
import com.ukefu.webim.web.model.PbxHost;
import com.ukefu.webim.web.model.PbxHostOrgiRela;

@Controller
@RequestMapping("/admin/mycallcenter")
public class MyCallCenterController extends Handler{
	
	@Autowired
	private PbxHostRepository pbxHostRes ;

	@Autowired
	private PbxHostOrgiRelaRepository pbxHostOrgiRelaRepository ;

	
	@RequestMapping(value = "/index")
    @Menu(type = "callcenter" , subtype = "mycallcenter" , access = false )
    public ModelAndView index(ModelMap map , HttpServletRequest request , @Valid String msg) {
		List<PbxHostOrgiRela> pbxHostOrgiRelaList = pbxHostOrgiRelaRepository.findByOrgi(super.getOrgi(request)) ;
		List<PbxHost> pbxHostList = new ArrayList<>();
		if(pbxHostOrgiRelaList != null && pbxHostOrgiRelaList.size() > 0){
			for(PbxHostOrgiRela pbxHostOrgiRela : pbxHostOrgiRelaList){
				PbxHost pbxHost = pbxHostRes.findById(pbxHostOrgiRela.getPbxhostid());
				if(pbxHost != null){
					pbxHostList.add(pbxHost);
				}
			}
		}
		if(UKDataContext.model.get("callcenter")!=null){
			CallCenterInterface callCenterImpl = (CallCenterInterface) UKDataContext.getContext().getBean("callcenter") ;
			
			for(PbxHost pbxHost : pbxHostList){
				if(callCenterImpl!=null){
					pbxHost.setConnected(callCenterImpl.connected(pbxHost.getId()));
				}
			}
		}
		map.addAttribute("pbxHostList" , pbxHostList);
		map.addAttribute("ismy" , true);
    	return request(super.createAdminTempletResponse("/admin/mycallcenter/index"));
    }
	
	@RequestMapping(value = "/pbxhost")
    @Menu(type = "callcenter" , subtype = "pbxhost" , access = false )
    public ModelAndView pbxhost(ModelMap map , HttpServletRequest request) {
		List<PbxHostOrgiRela> pbxHostOrgiRelaList = pbxHostOrgiRelaRepository.findByOrgi(super.getOrgi(request)) ;
		List<PbxHost> pbxHostList = new ArrayList<>();
		if(pbxHostOrgiRelaList != null && pbxHostOrgiRelaList.size() > 0){
			for(PbxHostOrgiRela pbxHostOrgiRela : pbxHostOrgiRelaList){
				PbxHost pbxHost = pbxHostRes.findById(pbxHostOrgiRela.getPbxhostid());
				if(pbxHost != null){
					pbxHostList.add(pbxHost);
				}
			}
		}
		if(UKDataContext.model.get("callcenter")!=null){
			CallCenterInterface callCenterImpl = (CallCenterInterface) UKDataContext.getContext().getBean("callcenter") ;
			
			for(PbxHost pbxHost : pbxHostList){
				if(callCenterImpl!=null){
					pbxHost.setConnected(callCenterImpl.connected(pbxHost.getId()));
				}
			}
		}
		map.addAttribute("pbxHostList" , pbxHostList);
    	return request(super.createRequestPageTempletResponse("/admin/mycallcenter/pbxhost/index"));
    }
	
}
