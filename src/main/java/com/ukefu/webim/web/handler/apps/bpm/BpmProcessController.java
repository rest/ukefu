package com.ukefu.webim.web.handler.apps.bpm;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.snaker.engine.SnakerEngineFacets;
import org.snaker.engine.SnakerHelper;
import org.snaker.engine.access.Page;
import org.snaker.engine.entity.Process;
import org.snaker.engine.helper.StreamHelper;
import org.snaker.engine.access.QueryFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ukefu.util.Menu;
import com.ukefu.webim.service.repository.MetadataRepository;
import com.ukefu.webim.service.repository.ProcessContentRepository;
import com.ukefu.webim.web.handler.Handler;
import com.ukefu.webim.web.model.ProcessContent;

@Controller
@RequestMapping("/apps/bpm")
public class BpmProcessController extends Handler{
	
	@Autowired
	private ProcessContentRepository processContentRes ;
	
	@Autowired
	private MetadataRepository metadataRes ;
	
	@Autowired
	private SnakerEngineFacets facets;
	
    @RequestMapping("/process")
    @Menu(type = "bpm" , subtype = "process" , access = false)
    public ModelAndView index(ModelMap map , HttpServletRequest request , @Valid String q) {
    	QueryFilter filter = new QueryFilter();
		if(!StringUtils.isBlank(q)) {
			filter.setDisplayName(q);
		}
    	Page<Process> page = new Page<Process>(getPs(request));
    	page.setPageNo(super.getP(request));
		facets.getEngine().process().getProcesss(page, filter);
		
		map.addAttribute("processList", page) ;
    	return request(super.createAppsTempletResponse("/apps/business/bpm/process"));
    }
    
    @RequestMapping(value="/process/undeploy")
    @Menu(type = "bpm" , subtype = "bpm" , access = false)
    public ModelAndView undeploy(HttpServletRequest request , @Valid String id){  
    	facets.getEngine().process().undeploy(id);
    	return request(super.createRequestPageTempletResponse("redirect:/apps/bpm/process.html"));
    }
    
    @RequestMapping(value="/process/redeploy")
    @Menu(type = "bpm" , subtype = "bpm" , access = false)
    public ModelAndView redeploy(HttpServletRequest request , @Valid String id){  
    	ProcessContent process = processContentRes.findByProcessidAndOrgi(id,super.getOrgi(request)) ;
    	if(process!=null && !StringUtils.isBlank(process.getProcessid())){
    		facets.getEngine().process().redeploy(id , StreamHelper.getStreamFromString(SnakerHelper.convertXml(process.getContent())));
    		process.setPublished(true);
    		processContentRes.save(process) ;
    	}
    	return request(super.createRequestPageTempletResponse("redirect:/apps/bpm/process.html"));
    }
    
    @RequestMapping("/process/delete")
    @Menu(type = "weixin" , subtype = "delete")
    public ModelAndView delete(ModelMap map , HttpServletRequest request , @Valid String id) {
    	if(!StringUtils.isBlank(id)){
    		facets.getEngine().process().cascadeRemove(id);
    	}
        return request(super.createRequestPageTempletResponse("redirect:/apps/bpm/process.html"));
    }
}