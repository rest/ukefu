package com.ukefu.util.webim;

import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import com.ukefu.util.UKTools;

public class WebIMClient {
	private String userid ;
	private String client ;
	private String sessionid ;
	private SseEmitter sse ;
	private long time ;
	private String orgi ;
	private String appid ;
	
	private String unikey ;
	
	public WebIMClient(String userid ,String orgi , String appid){
		this.userid = userid ;
		this.orgi = orgi ;
		this.appid = appid ;
		this.unikey = UKTools.getUniKey(userid, appid, orgi) ;
	}
	
	public WebIMClient(String userid ,String sessionid , String client , SseEmitter sse , long time ,String orgi , String appid){
		this.userid = userid ;
		this.sse = sse ;
		this.client = client ;
		this.time = time ;
		this.sessionid = sessionid;
		this.orgi = orgi ;
		this.appid = appid ;
		this.unikey = UKTools.getUniKey(userid, appid, orgi) ;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public SseEmitter getSse() {
		return sse;
	}
	public void setSse(SseEmitter sse) {
		this.sse = sse;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public String getSessionid() {
		return sessionid;
	}

	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}

	public String getOrgi() {
		return orgi;
	}

	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getUnikey() {
		return unikey;
	}

	public void setUnikey(String unikey) {
		this.unikey = unikey;
	}
}
