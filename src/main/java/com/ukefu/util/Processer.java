package com.ukefu.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

public class Processer {
	private static List<File> htmlFiles = new ArrayList<File>();
	public static void main(String[] args) throws IOException {
		listFiles(new File("D:\\wordspace\\program\\uckefu\\UCKeFu-WebIM\\src\\main\\resources\\templates"));
		
		for(File htmlFile : htmlFiles) {
			readFile(htmlFile);
		}
	}
	public static void listFiles(File dir) {
        if(dir.isDirectory()) {
            File next[]=dir.listFiles();
            for (File nextFile : next) {
                if(nextFile.getName().toLowerCase().endsWith(".html")) {
                	htmlFiles.add(nextFile);
                }
                if(nextFile.isDirectory()) {
                	listFiles(nextFile);
                }
            }
        }
    }
	
	public static void readFile(File htmlFile) throws IOException {
		String content = FileUtils.readFileToString(htmlFile) ;
		Pattern pattern = Pattern.compile("[\\u4E00-\\u9FA5A-Za-z0-9]+[\\u4E00-\\u9FA5\\u3002\\uff1f\\uff01\\uff0c\\u3001\\uff1b\\uff1a\\u201c\\u201d\\u2018\\u2019\\uff08\\uff09\\u300a\\u300b\\u3008\\u3009\\u3010\\u3011\\u300e\\u300f\\u300c\\u300d\\ufe43\\ufe44\\u3014\\u3015\\u2026\\u2014\\uff5e\\ufe4f\\uffe5]+");
		
	    Matcher matcher = pattern.matcher(content);
	    StringBuffer strb = new StringBuffer();
	    int i = 0 ;
	    while(matcher.find()) {
	    	String name = "callcenter.admin.area.add."+i ;
	    	Matcher temp = matcher.appendReplacement(strb,"<@spring name='"+name+"'>");
	    	i++;
	    	String text = content.substring(temp.start(), temp.end()) ;
	    	FileUtils.write(new File("d:\\data\\a.properies"), name+"=" + text+"\n", "UTF-8",true);
	    }
	    matcher.appendTail(strb) ;
	    
	    FileUtils.write(new File("d://data/a.html"), strb.toString());
	    FileUtils.write(new File("d://data/b.html"), content);
	}
}
