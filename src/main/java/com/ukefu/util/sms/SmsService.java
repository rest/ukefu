package com.ukefu.util.sms;

import java.util.Map;

import com.ukefu.webim.web.model.SmsResult;
import com.ukefu.webim.web.model.SmsTemplate;
import com.ukefu.webim.web.model.SystemMessage;

public interface SmsService {
	public SmsResult send(SystemMessage systemMessage , SmsResult result , String phone , SmsTemplate tp, Map<String, Object> values) throws Exception ;
	
	public SmsResult checkReach(SmsResult result) throws Exception ;
}
