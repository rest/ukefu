package me.chanjar.weixin.mp.api.impl;

import com.ukefu.webim.util.weixin.config.WechatOpenConfiguration;
import me.chanjar.weixin.common.WxType;
import me.chanjar.weixin.common.bean.WxAccessToken;
import me.chanjar.weixin.common.error.WxError;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.common.util.http.HttpType;
import me.chanjar.weixin.common.util.http.apache.ApacheHttpClientBuilder;
import me.chanjar.weixin.common.util.http.apache.DefaultApacheHttpClientBuilder;
import me.chanjar.weixin.mp.api.WxMpConfigStorage;
import me.chanjar.weixin.mp.api.WxMpService;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;

import java.io.IOException;
import java.util.concurrent.locks.Lock;

/**
 * apache http client方式实现.
 */
public class WxMpServiceHttpClientImpl extends BaseWxMpServiceImpl<CloseableHttpClient, HttpHost> {
  private CloseableHttpClient httpClient;
  private HttpHost httpProxy;

  @Override
  public CloseableHttpClient getRequestHttpClient() {
    return httpClient;
  }

  @Override
  public HttpHost getRequestHttpProxy() {
    return httpProxy;
  }

  @Override
  public HttpType getRequestType() {
    return HttpType.APACHE_HTTP;
  }

  @Override
  public void initHttp() {
    WxMpConfigStorage configStorage = this.getWxMpConfigStorage();
    ApacheHttpClientBuilder apacheHttpClientBuilder = configStorage.getApacheHttpClientBuilder();
    if (null == apacheHttpClientBuilder) {
      apacheHttpClientBuilder = DefaultApacheHttpClientBuilder.get();
    }

    apacheHttpClientBuilder.httpProxyHost(configStorage.getHttpProxyHost())
      .httpProxyPort(configStorage.getHttpProxyPort())
      .httpProxyUsername(configStorage.getHttpProxyUsername())
      .httpProxyPassword(configStorage.getHttpProxyPassword());

    if (configStorage.getHttpProxyHost() != null && configStorage.getHttpProxyPort() > 0) {
      this.httpProxy = new HttpHost(configStorage.getHttpProxyHost(), configStorage.getHttpProxyPort());
    }

    this.httpClient = apacheHttpClientBuilder.build();
  }

  private WechatOpenConfiguration wechatOpenConfiguration;

  @Override
  public String getAccessToken(boolean forceRefresh) throws WxErrorException {
    Lock lock = this.getWxMpConfigStorage().getAccessTokenLock();
    try {
      lock.lock();
      //secret为空则判断为二维码 授权的
      if(StringUtils.isBlank(this.getWxMpConfigStorage().getSecret()) ){
        if(this.getWxMpConfigStorage().isAccessTokenExpired() || forceRefresh) {
          String token = WechatOpenConfiguration.getInstance().getWxOpenComponentService().getAuthorizerAccessToken(this.getWxMpConfigStorage().getAppId(), true);
          System.out.println(token);
          WxAccessToken accessToken = new WxAccessToken();
          accessToken.setAccessToken(token);
          accessToken.setExpiresIn(7200);
          this.getWxMpConfigStorage().updateAccessToken(accessToken.getAccessToken(),
                  accessToken.getExpiresIn());
        }
      }else{
        if (this.getWxMpConfigStorage().isAccessTokenExpired() || forceRefresh) {
          String url = String.format(WxMpService.GET_ACCESS_TOKEN_URL,
                  this.getWxMpConfigStorage().getAppId(), this.getWxMpConfigStorage().getSecret());
          try {
            HttpGet httpGet = new HttpGet(url);
            if (this.getRequestHttpProxy() != null) {
              RequestConfig config = RequestConfig.custom().setProxy(this.getRequestHttpProxy()).build();
              httpGet.setConfig(config);
            }
            try (CloseableHttpResponse response = getRequestHttpClient().execute(httpGet)) {
              String resultContent = new BasicResponseHandler().handleResponse(response);
              WxError error = WxError.fromJson(resultContent, WxType.MP);
              if (error.getErrorCode() != 0) {
                throw new WxErrorException(error);
              }
              WxAccessToken accessToken = WxAccessToken.fromJson(resultContent);
              this.getWxMpConfigStorage().updateAccessToken(accessToken.getAccessToken(),
                      accessToken.getExpiresIn());
            } finally {
              httpGet.releaseConnection();
            }
          } catch (IOException e) {
            throw new RuntimeException(e);
          }
        }
      }

    } finally {
      lock.unlock();
    }
    return this.getWxMpConfigStorage().getAccessToken();
  }
}
